#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>

#include "config.h"
#include "logger.h"
#include "socket_server.h"
#include "pthread.h"
#include "processer.h"
#include "fish_parser.h"
#include "aquarium.h"
#include "parser_cmd.h"
#include "signal.h"

#define BUFFER_PROMPT 256


void handle_pipe_broken(int sig) {
    log_warning("A client has broken a writing pipe.");
}

int main(int argc, char *argv[])
{
    init();


    pthread_t thread_server;

    signal(SIGPIPE, handle_pipe_broken);

    pthread_create(&thread_server, NULL, (void *(*) (void *))socket_server_start, request_processer);
    init_method_list(); // Init fisher protocole parser

    pthread_t thread_fish_manager;

    pthread_create(&thread_fish_manager, NULL, (void*(*) (void*))start_fish_manager, NULL);

    // Handle command prompt
    char* buff = calloc(sizeof(char), BUFFER_PROMPT);

    for(;;) {
        if(read(STDIN_FILENO, buff, BUFFER_PROMPT) == -1) {
            log_error("An error occured while trying to read your command.");
        }

        handle_cmd(buff);

        for (size_t i = 0; i < BUFFER_PROMPT; i++)
        {
            buff[i] = 0;
        }

    }

    return EXIT_SUCCESS;
}