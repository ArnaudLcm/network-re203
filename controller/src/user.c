#include "user.h"
#include "utils.h"
#include "aquarium.h"
#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>
#include <unistd.h>
#include "semaphore.h"
#include "logger.h"
#include "config.h"
#include "view.h"
#include "linked_list.h"

sem_t semaphore_users;


node *get_user_by_fd(int fd)
{
    if (aqua == NULL)
    {
        return NULL;
    }



    node* current_node_user = aqua->list_users->head;


    while (current_node_user != NULL)
    {
        if (current_node_user->value != NULL && ((user*)current_node_user->value)->fd == fd)
        {
            return current_node_user;
        }

        current_node_user = current_node_user->next;
    }

    return NULL;
}

bool add_user(int fd)
{
    sem_wait(&semaphore_users);


    node *usr = get_user_by_fd(fd); // Check if the user already exists

    if (usr != NULL)
    {
        return FALSE;
    }

    node *n_usr = malloc(sizeof(node));
    n_usr->next = NULL;
    n_usr->previous = NULL;
    n_usr->value = malloc(sizeof(user));

    user* new_user = n_usr->value;

    new_user->instructions = malloc(sizeof(node_user_instruction));
    new_user->getting_fishes_continuously = FALSE;

    new_user->fd = fd;
    struct timeval tv;
    gettimeofday(&tv, NULL);
    new_user->updated_at = tv.tv_sec;

    new_user->instructions = NULL;



    add_node(aqua->list_users, n_usr);

    sem_post(&semaphore_users);
    return TRUE;
}

bool remove_user(int fd)
{

    node *user_nd = get_user_by_fd(fd);

    del_node(aqua->list_users, user_nd);

    remove_owner(fd);

    close(fd);


    return TRUE;
}

bool remove_user_safely(int fd)
{   
    sem_wait(&semaphore_users);
    bool validity = remove_user(fd);
    sem_post(&semaphore_users);

    return validity;
}


bool is_user_timedout(node* nd_user) {
    if(nd_user != NULL) {
        struct timeval tv;
        gettimeofday(&tv, NULL);
        if((tv.tv_sec - ((user*)nd_user->value)->updated_at) > TIMEOUT_USER) {
            return TRUE;
        }


    }

    return FALSE;

}



void free_users() {
    node* current_user = aqua->list_users->tail;
    while(current_user != NULL && current_user->previous != NULL) {
        current_user = current_user->previous;
        close(((user*)current_user->value)->fd);
        free_user_instructions(current_user->value);
        
        free(current_user->next);
    }

    close(((user*)current_user->value)->fd);
    free(current_user);
}

bool push_instruction(user* usr, user_instruction* istr) {

    node_user_instruction* nd_ui = malloc(sizeof(node_user_instruction));
    nd_ui->value = istr;
    nd_ui->next = NULL;
    nd_ui->previous = NULL;

    if(usr->instructions == NULL) { // The user has not instruction yet
        usr->instructions = nd_ui;
    } else {
        nd_ui->next = usr->instructions;
        usr->instructions->previous = nd_ui;
        usr->instructions = nd_ui;

    }

    return TRUE;
}

node_user_instruction* pop_instruction(user* usr) {
    if(usr->instructions == NULL) {
        return NULL;
    }
    node_user_instruction* current_nd_instruction = usr->instructions;
    while(current_nd_instruction != NULL && current_nd_instruction->next != NULL) { // let's find the tail
        current_nd_instruction = current_nd_instruction->next;
    }

    if(current_nd_instruction->previous != NULL) {
        current_nd_instruction->previous->next = NULL;
    } else { // It's the head !
        usr->instructions = NULL;
    }

    return current_nd_instruction;
}

void free_node_instruction(node_user_instruction* nd_u) {

    free(nd_u->value->fish_name);

    free(nd_u->value);

    free(nd_u);

}

void free_user_instructions(user* user) {

    node_user_instruction* current_instruction = user->instructions;

    while(current_instruction != NULL && current_instruction->next != NULL) {
        current_instruction = current_instruction->next;
        free_node_instruction(current_instruction->previous);
    }

    free_node_instruction(current_instruction);

}