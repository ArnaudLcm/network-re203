#ifndef __FISH_H__
#define __FISH_H__

#include "semaphore.h"
#include "utils.h"
#include "linked_list.h"
#include "movements.h"

extern sem_t semaphore_fishs;

enum fish_status {
    NOT_STARTED,
    STARTED
};

typedef struct {
    char* name;
    position pos;
    int width;
    int height;
    int mobility;
    int status;
} fish;

void free_fishs();



bool add_fish(char* id, int x, int y, int width, int height, char* method, int client_fd);

node* get_fish_by_id(char* id);

bool remove_fish(char* id);

#endif // !__FISH_H__