#ifndef __CONTROLLERS_H__
#define __CONTROLLERS_H__
#include "fish_parser.h"

int add_fish_controller(fish_instruction* fi, int client_fd);


int del_fish_controller(fish_instruction* fi, int client_fd);

int start_fish_controller(fish_instruction* fi, int client_fd);

int ping_controller(fish_instruction* fi, int client_fd);

int ls_controller(fish_instruction* fi, int client_fd);

int log_controller(fish_instruction* fi, int client_fd);

int get_fishes_controller(fish_instruction* fi, int client_fd);

int hello_controller(char* id, int client_fd);


int get_fishes_continuously_controller(fish_instruction* fi, int client_fd);

#endif // !__CONTROLLERS_H__

