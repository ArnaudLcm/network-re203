#include "socket_server.h"
#include "logger.h"
#include "fish_parser.h"
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <stdarg.h>
#include "controllers.h"
#include "middlewares.h"
#include "processer.h"
#include <stdarg.h>

/**
 * @brief Use vasprintf to format the input string before calling send_response
 */
void send_formatted_response(int fd, char *format, ...)
{
    va_list args;
    va_start(args, format);

    char *s;
    vasprintf(&s, format, args);

    send_response(s, fd);

    free(s);
}

/**
 * @brief Send a response by writing on the socket filedescriptor associated to the request
 */
void send_response(char *response, int fd)
{
    send_partial(response, fd);
    if (write(fd, "\n", 1) == -1)
    {
        log_error("An error occured while trying to send the end of a response to a specific client.");
        return;
    }
}

/**
 * @brief This function is a dumb send to client, prefer using send_response if you want it to be formatted (with an ending \n)
 * It also uses vasprintf to format input
*/
void send_formatted_partial(int fd, char* format, ...) {
    va_list args;
    va_start(args, format);

    char *s;
    vasprintf(&s, format, args);

    send_partial(s, fd);

    free(s);
}

/**
 * @brief This function is a dumb send to client, prefer using send_response if you want it to be formatted (with an ending \n)
*/
void send_partial(char* to_send, int fd) {
    int size_to_write = (strlen(to_send) + 1) * sizeof(char);
    int written_size = 0;

    while (written_size < size_to_write)
    {
        int ret = write(fd, to_send + written_size, size_to_write);
        if (ret == -1)
        {
            log_error("An error occured while trying to send a response to a specific client.");
            return;
        }
        written_size += ret;
    }
}


void send_error_response(int fd)
{
    int err = write(fd, "NOK\n", (5) * sizeof(char));
    if (err < 0)
    {
        log_error("An error occured while trying to send a response to a specific client.");
    }
}

/**
 * @brief This function is triggered every time the socket server receives a new request.
 * Thus, the purpose of this function is to parse the body of the fish protocol and root it to an adapted controller.
 */
int request_processer(char *input, int client_fd, int server_error_code)
{
    if (server_error_code != SE_NONE)
    {
        char *error_message = "Server-side error : Unable to treat request.";
        if (write(client_fd, error_message, (strlen(error_message) + 1) * sizeof(char)) < 0)
            return -1;
        return 0;
    }
    // If the request is an empty string, we simply ignore it
    if (!strlen(input))
    {
        return 0;
    }
    log_debug("Client sent request (strlen: %d): \"%s\"", strlen(input), input);

    // Parse the request

    fish_instruction *fi = malloc(sizeof(fish_instruction));
    fi->args = malloc(sizeof(token) * NUMBER_OF_ARGS);
    fi->args_size = 0;
    lexxer(fi, input);

    if (fi->args_size == 0)
    {
        send_error_response(client_fd);
        return 0;
    }

    // Rooting request

    if (fi->args[0].name == STR)
    {
        if (strcmp(fi->args[0].value, "addFish") == 0)
        {
            if (is_token_list_valid(fi, 12, STR, STR, STR, NBR, OP, NBR, OP, NBR, OP, NBR, OP, STR))
            {
                return auth_required_middleware(add_fish_controller, client_fd, fi);
            }
        }
        else if (strcmp(fi->args[0].value, "delFish") == 0)
        {
            if (is_token_list_valid(fi, 2, STR, STR))
            {
                return auth_required_middleware(del_fish_controller, client_fd, fi);
            }
        }
        else if (strcmp(fi->args[0].value, "startFish") == 0)
        {
            if (is_token_list_valid(fi, 2, STR, STR))
            {
                return auth_required_middleware(start_fish_controller, client_fd, fi);
            }
        }
        else if (strcmp(fi->args[0].value, "ping") == 0)
        {
            if (is_token_list_valid(fi, 2, STR, NBR))
            {
                return auth_required_middleware(ping_controller, client_fd, fi);
            }
        }
        else if (strcmp(fi->args[0].value, "log") == 0)
        {
            if (is_token_list_valid(fi, 2, STR, STR))
            {
                return auth_required_middleware(log_controller, client_fd, fi);
            }
        }
        else if (strcmp(fi->args[0].value, "getFishesContinuously") == 0)
        {
            return auth_required_middleware(get_fishes_continuously_controller, client_fd, fi);
        }
        else if (strcmp(fi->args[0].value, "ls") == 0)
        {
            return ls_controller(fi, client_fd);
        }
        else if (strcmp(fi->args[0].value, "getFishes") == 0)
        {
            return auth_required_middleware(get_fishes_controller, client_fd, fi);
        }
        else if (strcmp(fi->args[0].value, "hello") == 0)
        {
            if (fi->args_size == 1 && is_token_list_valid(fi, 1, STR))
            {
                return hello_controller(NULL, client_fd);
            }
            if (fi->args_size == 4 && is_token_list_valid(fi, 4, STR, STR, STR, STR))
            {
                return hello_controller(fi->args[3].value, client_fd);
            }
        }
    }

    send_error_response(client_fd); // No root found

    return 0;
}