#include "stdio.h"
#include "stdlib.h"
#include <sys/stat.h>
#include <fcntl.h>
#include <string.h>
#include "logger.h"
#include "fish_parser.h"
#include "aquarium.h"
#include <unistd.h>
#include "math.h"
#include <pthread.h>
#include <semaphore.h>
#include "processer.h"
#include "utils.h"
#include "fcntl.h"
#include "config.h"
#include "aquarium.h"
#include "view.h"
#include "movements.h"
#include "linked_list.h"
#include "user.h"
#include "fish.h"

char *path_to_config_folder = "config/";
char *config_ext = ".cfg";
aquarium *aqua = NULL;

void free_aquarium()
{
    if (aqua)
    {
        free_users();
        free_views();
        free_fishs();
        free(aqua);
    }
}

bool is_aquarium_load()
{
    if (aqua == NULL)
    {
        return FALSE;
    }

    return TRUE;
}

int show_aquarium()
{

    if (!is_aquarium_load())
    {
        return FALSE;
    }
    log_info("%dx%d", aqua->width, aqua->height);
    node *current_node = aqua->list_view->head;
    while (current_node != NULL)
    {
        view *current_view = current_node->value;
        log_info("%s %dx%d+%d+%d", current_view->id, current_view->x, current_view->y, current_view->width, current_view->height);
        current_node = current_node->next;
    }
    return TRUE;
}

/**
 * The purpose of this function is to store in a config file the current aquarium config.
 */
bool save(char *aquarium_name)
{
    char *path = calloc(sizeof(char), strlen(aquarium_name) + strlen(path_to_config_folder) + strlen(config_ext) + 1);
    strcat(path, path_to_config_folder);
    strcat(path, aquarium_name);
    strcat(path, config_ext);
    int fd = open(path, O_RDWR);

    if (fd != -1)
    {
        log_warning("An other configuration with the name %s already exists.", aquarium_name);
        return FALSE;
    }

    fd = open(path, O_WRONLY | O_APPEND | O_CREAT, 0644);

    if (fd == -1)
    {
        log_error("An error ocurred while trying to register the config file.");
        close(fd);
        return FALSE;
    }

    char *aqua_width = calloc(sizeof(char), 5);
    char *aqua_height = calloc(sizeof(char), 5);
    sprintf(aqua_width, "%d", aqua->width);
    sprintf(aqua_height, "%d", aqua->height);
    int size_to_write = strlen(aqua_width) + 1 + strlen(aqua_height) + 1;
    char *aqua_metadata = calloc(sizeof(char), size_to_write);
    str_multiple_cpy(aqua_metadata, 4, aqua_width, "x", aqua_height, "\n");

    write(fd, aqua_metadata, size_to_write);

    node *current_node = aqua->list_view->head;

    while (current_node != NULL)
    {
        view *current_view = current_node->value;
        char *pos_x = itoa(current_view->x);
        char *pos_y = itoa(current_view->y);
        char *width = itoa(current_view->width);
        char *height = itoa(current_view->height);
        size_to_write = strlen(current_view->id) + strlen(width) + strlen(height) + strlen(pos_x) + strlen(pos_y) + 1 + 1 + 3;

        char *aqua_view = calloc(sizeof(char), size_to_write);

        str_multiple_cpy(aqua_view, 10, current_view->id, " ", pos_x, "x", pos_y, "+", width, "+", height, "\n");

        write(fd, aqua_view, size_to_write);

        free(aqua_view);

        current_node = current_node->next;

        free(pos_x);
        free(pos_y);
        free(width);
        free(height);
    }

    close(fd);

    log_success("Configuration %s successfuly saved.", aquarium_name);

    return TRUE;
}

/**
 * @brief The purpose of this function is to load an aquarium config by reading the associated config file.
 */
bool load(char *aquarium_name)
{

    char *path = calloc(sizeof(char), strlen(aquarium_name) + strlen(path_to_config_folder) + strlen(config_ext) + 1);
    str_multiple_cpy(path, 3, path_to_config_folder, aquarium_name, config_ext);

    sem_destroy(&semaphore_users);
    sem_destroy(&semaphore_views);

    FILE *fd;
    char *line = NULL;
    fd = fopen(path, "r");
    size_t len = 0;
    size_t len_read;
    size_t nbr_line = 0;
    if (fd == NULL)
    {
        log_warning("Can't open file : %s", path);
        return FALSE;
    }

    if (aqua != NULL)
    { // Memory free the previous aquarium
        free_aquarium();
    }

    aqua = malloc(sizeof(aquarium));

    aqua->list_view = malloc(sizeof(linked_list));
    aqua->list_view->head = NULL;
    aqua->list_view->tail = NULL;

    aqua->list_users = malloc(sizeof(linked_list));
    aqua->list_users->head = NULL;
    aqua->list_users->tail = NULL;

    aqua->list_fishs = malloc(sizeof(linked_list));
    aqua->list_fishs->head = NULL;
    aqua->list_fishs->tail = NULL;

    while ((len_read = getline(&line, &len, fd)) != -1)
    {

        fish_instruction *fi = malloc(sizeof(fish_instruction));
        fi->args = malloc(sizeof(token) * NUMBER_OF_ARGS);
        fi->args_size = 0;

        lexxer(fi, line);

        if (nbr_line == 0) // Print aquarium information
        {
            if (fi->args_size != 3 || !is_token_list_valid(fi, 3, NBR, OP, NBR))
            {
                log_warning("Invalid aquarium config.");
                return FALSE;
            }
            aqua->height = *(int *)fi->args[2].value;
            aqua->width = *(int *)fi->args[0].value;
        }
        else // Print views information
        {

            if (fi->args_size != 8 || !is_token_list_valid(fi, 8, STR, NBR, OP, NBR, OP, NBR, OP, NBR))
            {
                log_warning("Invalid aquarium config.");
                free_aquarium();
                return FALSE;
            }

            if (does_view_already_exist((char *)fi->args[0].value))
            {
                log_warning("%s is already used in the config.", (char *)fi->args[0].value);
                free_aquarium();
                return FALSE;
            }

            node *new_node = malloc(sizeof(node));
            new_node->next = NULL;
            new_node->previous = NULL;
            new_node->value = malloc(sizeof(view));
            view *current_view = new_node->value;
            current_view->id = calloc(sizeof(char), strlen((char *)fi->args[0].value) + 1);
            strcpy(current_view->id, (char *)fi->args[0].value);
            current_view->x = *(int *)fi->args[1].value;
            current_view->y = *(int *)fi->args[3].value;
            current_view->width = *(int *)fi->args[5].value;
            current_view->height = *(int *)fi->args[7].value;

            current_view->owner = NO_OWNER;

            add_node(aqua->list_view, new_node);
        }
        nbr_line++;

        free_instruction_fish(fi);
    }

    fclose(fd);
    if (line)
        free(line);

    sem_init(&semaphore_users, PTHREAD_PROCESS_SHARED, 1);
    sem_init(&semaphore_fishs, PTHREAD_PROCESS_SHARED, 1);
    sem_init(&semaphore_views, PTHREAD_PROCESS_SHARED, 1);

    log_success("%s has been successfuly loaded.", aquarium_name);

    return TRUE;
}

/**
 * @brief The purpose of this function is to watch every x seconds for started fish and update their positions in
 * the aquarium
 */
void start_fish_manager()
{

    for (;;)
    {

        if (aqua != NULL)
        {
            node *current_node = aqua->list_fishs->head;
            while (current_node != NULL)
            {

                fish *fish = current_node->value;
                position p = {.x = fish->pos.x, .y = fish->pos.y};
                if (fish->status == STARTED)
                { // Find a started fish
                    node *nd_v = aqua->list_view->head;

                    p = get_position_from_method(fish->mobility, fish->pos);

                    while (nd_v != NULL)
                    { // Loop throught views

                        view *view = nd_v->value;

                        position viewPos = {.x = view->x, .y = view->y};
                        if (is_view_concerned_by_fish(p, fish->pos, viewPos, view->width, view->height))
                        {

                            sem_wait(&semaphore_users);
                            node *nd_u = get_user_by_fd(view->owner);
                            if (nd_u != NULL)
                            {
                                if (is_user_timedout(nd_u))
                                {
                                    log_info("User %d has been kicked due to keep alive timedout.", ((user *)nd_u->value)->fd);
                                    send_error_response(((user *)nd_u->value)->fd);
                                    remove_user(((user *)nd_u->value)->fd);
                                    sem_post(&semaphore_users);
                                    continue;
                                }

                                log_debug("COMPARE: fish->pos: {%d, %d} p: {%d, %d}", fish->pos.x, fish->pos.y, p.x, p.y);
                                user_instruction *instruction_init = malloc(sizeof(user_instruction));
                                instruction_init->x = (int)(((fish->pos.x - view->x) * 100) / view->width);
                                instruction_init->y = (int)(((fish->pos.y - view->y) * 100) / view->height);
                                instruction_init->speed = 0;
                                instruction_init->width = fish->width;
                                instruction_init->height = fish->height;
                                instruction_init->fish_name = calloc(sizeof(char), strlen(fish->name) + 1);
                                strcat(instruction_init->fish_name, fish->name); // Copy the fish id
                                push_instruction(nd_u->value, instruction_init);

                                user_instruction *instruction = malloc(sizeof(user_instruction));
                                instruction->x = (int)(((p.x - view->x) * 100) / view->width);
                                instruction->y = (int)(((p.y - view->y) * 100) / view->height);
                                instruction->speed = 5;
                                instruction->width = fish->width;
                                instruction->height = fish->height;
                                instruction->fish_name = calloc(sizeof(char), strlen(fish->name) + 1);
                                strcat(instruction->fish_name, fish->name); // Copy the fish id
                                push_instruction(nd_u->value, instruction);

                                log_info("A new instruction has been pushed to user %d", ((user *)nd_u->value)->fd);
                            }

                            sem_post(&semaphore_users);
                        }

                        nd_v = nd_v->next;
                    }
                    fish->pos = p;
                }

                current_node = current_node->next;
            }

            updateContinuousListeners();
        }

        sleep(UPDATE_FISHS_POSITIONS);
    }
}

/**
 * @brief Send fishes location to all clients that are currently executing getFishesContinuously
 */
void updateContinuousListeners()
{
    log_debug("%s called", __func__);
    node *node_user = aqua->list_users->tail;
    while (node_user)
    {
        user *user = node_user->value;
        if (is_user_timedout(node_user))
        {
            log_warning("User (view id: %s) timed out", ((view *)get_view_by_owner(user->fd)->value)->id);
            send_error_response(user->fd);
            sem_wait(&semaphore_users);

            remove_user(user->fd);
            sem_post(&semaphore_users);
        }
        else
        {
            if (user->getting_fishes_continuously)
            {
                send_formatted_partial(user->fd, "list ");
                struct node_user_instruction *instruction_node = pop_instruction(user);
                while (instruction_node)
                {
                    user_instruction *instr = instruction_node->value;
                    send_formatted_partial(user->fd, "[%s at %dx%d,%dx%d,%d] ", instr->fish_name, instr->x, instr->y, instr->width, instr->height, instr->speed);
                    log_debug("[%s at %dx%d,%dx%d,%d] ", instr->fish_name, instr->x, instr->y, instr->width, instr->height, instr->speed);
                    instruction_node = pop_instruction(user);
                }
                send_formatted_response(user->fd, "");
            }
        }
        node_user = node_user->previous;
    }
}