#ifndef __FISH_PARSER_H__
#define __FISH_PARSER_H__

#define REQUEST_BUFFER_SIZE 256
#define NUMBER_OF_ARGS 20

void init_method_list();


enum token_kind {
    NBR,
    STR,
    OP
};

enum parser_response {
    FP_SUCCESS,
    FP_UNKNOWN_ACTION,
    FP_TOO_MUCH_ARGS
};

typedef struct {
    void* value;
    int name; // enum token_kind
} token;

typedef struct {
    token* args;
    int args_size;
} fish_instruction;

void free_instruction_fish(fish_instruction* fi);
int is_token_list_valid(fish_instruction* fi, int nbr_tokens, ...);

int lexxer(fish_instruction* fi, char* buffer);
#endif