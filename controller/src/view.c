#include "utils.h"
#include "view.h"
#include "aquarium.h"
#include <stdio.h>
#include "fcntl.h"
#include <stdlib.h>
#include "semaphore.h"
#include <string.h>
#include "logger.h"
#include "processer.h"
#include "linked_list.h"

sem_t semaphore_views;

bool take_view(char *id, int new_owner)
{
    if (!is_aquarium_load())
    {
        return FALSE;
    }
    sem_wait(&semaphore_views);

    if (!does_view_already_exist(id))
    {
        sem_post(&semaphore_views);
        return FALSE;
    }

    node *current_node = get_view_by_id(id);

    if (((view *)current_node->value)->owner != -1)
    {
        sem_post(&semaphore_views);
        return FALSE;
    }

    ((view *)current_node->value)->owner = new_owner;

    sem_post(&semaphore_views);

    return TRUE;
}

bool remove_owner(int fd)
{

    if (!is_aquarium_load())
    {
        return FALSE;
    }
    sem_wait(&semaphore_views);

    node *current_node = get_view_by_owner(fd);

    if (current_node == NULL)
    {
        sem_post(&semaphore_views);

        return FALSE;
    }
    ((view *)current_node->value)->owner = NO_OWNER;

    sem_post(&semaphore_views);

    return TRUE;
}

node *get_view_by_owner(int fd)
{
    node *current_node = aqua->list_view->head; // Head of the list

    while (current_node != NULL)
    { // Find the node in the list
        if (((view *)current_node->value)->owner == fd)
        {
            return current_node;
        }
        current_node = current_node->next;
    }

    return NULL;
}

node *get_view_by_id(char *id)
{

    node *current_node = aqua->list_view->head; // Head of the list

    while (current_node != NULL)
    { // Find the node in the list
        if (strcmp(((view *)current_node->value)->id, id) == 0)
        {
            return current_node;
        }
        current_node = current_node->next;
    }

    return NULL;
}

node *find_available_view()
{

    if (is_aquarium_load())
    {
        sem_wait(&semaphore_views);
        node *current_node = aqua->list_view->head; // Head of the list

        while (current_node != NULL)
        {
            if (((view *)current_node->value)->owner == -1)
            {
                sem_post(&semaphore_views);
                return current_node;
            }
            else
            {
                if (!is_valid_fd(((view *)current_node->value)->owner))
                {                                                             // User disconnected
                    remove_user_safely(((view *)current_node->value)->owner); // Also remove the user
                    ((view *)current_node->value)->owner = -1;
                    sem_post(&semaphore_views);
                    return current_node;
                }
            }
            current_node = current_node->next;
        }
        sem_post(&semaphore_views);
        return NULL;
    }
    return NULL;
}

/**
 * @brief Find the node with a specific id in the linked view list
 */
bool does_view_already_exist(char *id)
{
    if (is_aquarium_load())
    {
        node *current_node = aqua->list_view->head;
        while (current_node != NULL)
        {
            if (current_node->value && ((view *)current_node->value)->id && strcmp(((view *)current_node->value)->id, id) == 0)
            {
                return TRUE;
            }

            current_node = current_node->next;
        }
        return FALSE;
    }

    return FALSE;
}

void free_views()
{
    if (aqua == NULL)
    {
        return; // Should not append
    }
    node *current_node = aqua->list_view;
    while (current_node != NULL && current_node->previous != NULL)
    {
        current_node = current_node->previous;
        free(current_node->next);
    }

    free(current_node);
}

/**
 * @brief The aquarium view lists is a double linked list. As a consequence, when the delete view command is triggered,
 * we have to unlink the associated node.
 */
bool remove_view_by_id(char *id)
{

    node *current_node = get_view_by_id(id);
    if (current_node == NULL)
    {
        log_debug("It seems that %s doesn't exist anymore.", id);
        return FALSE;
    }

    view *current_view = (view *)current_node->value;

    if (current_view->owner != -1)
    {
        send_response("view deleted\n", current_view->owner);
        remove_owner(current_view->owner);
    }

    del_node(aqua->list_view ,current_node);

    free(current_node->value); // @TODO : FInd a better way
    free(current_node);
    log_success("%s has been successfuly deleted.", id);
    return TRUE;
}