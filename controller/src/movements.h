#ifndef __MOVEMENTS_H__
#define __MOVEMENTS_H__
#include <stdio.h>
#include <stdlib.h> 
#include <time.h>
#include "view.h"
#include "utils.h"

enum mobility_model {
    RandomWayPoint,
    Horizontal,
    Vertical,
    Diagonal,
    __MOBILITY_LENGTH
};

position get_position_from_method(int movement, position fish_position);

position get_random_position_from_aquarium();



position get_relative_position_in_view(int absolute_x, int absolute_y, view* v);

position get_absolute_position_from_view_pos(int relative_x, int relative_y, view* v);


#endif // !__MOVEMENTS_H__