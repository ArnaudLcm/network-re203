#ifndef __MIDDLEWARE_H__
#define __MIDDLEWARE_H__
#include "fish_parser.h"

int auth_required_middleware(int (*next) (fish_instruction *, int), int fd, fish_instruction* fi);

#endif // !__MIDDLEWARE_H__