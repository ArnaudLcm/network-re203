#include "movements.h"
#include "aquarium.h"
#include "view.h"
#include "logger.h"


/**
 * @brief Helper used by RandWayPoint movement method to get a new random position in the aquarium
*/
position get_random_position_from_aquarium() {
    position pos;

    if(aqua == NULL) {
        pos.x = -1;
        pos.y = -1;
        return pos;
    }

    srand( time( NULL ) );

    pos.x = rand()%aqua->width;
    pos.y = rand()%aqua->height;

    return pos;

}

position get_position_for_horizontal_movement(position initial) {
    position pos;

    if(aqua == NULL) {
        pos.x = -1;
        pos.y = -1;
        return pos;
    }

    if(initial.x == aqua->width) {
        pos.x = 0;
        pos.y = initial.y;
    } else {
        pos.x = aqua->width;
        pos.y = initial.y;
    }

    return pos;
}

position get_position_for_vertical_movement(position initial) {
    position pos;

    if(aqua == NULL) {
        pos.x = -1;
        pos.y = -1;
        return pos;
    }

    if(initial.y == aqua->height) {
        pos.x = initial.x;
        pos.y = 0;
    } else {
        pos.x = initial.x;
        pos.y = aqua->height;
    }

    return pos;
}
position get_position_for_diagonal_movement(position initial) {
    position pos;

    if(aqua == NULL) {
        pos.x = -1;
        pos.y = -1;
        return pos;
    }

    if(initial.x == 0) {
        pos.x = aqua->width;
        pos.y = aqua->height;
    } else {
        pos.x = 0;
        pos.y = 0;
    }

    return pos;
}

/**
 * @brief Movement factory
*/
position get_position_from_method(int movement, position fish_position) {

    switch (movement)
    {
    case 0:
        return get_random_position_from_aquarium();
        break;
    
    case 1:
        return get_position_for_horizontal_movement(fish_position);
        break;
    case 2:
        return get_position_for_vertical_movement(fish_position);
        break;
    case 3:
        return get_position_for_diagonal_movement(fish_position);
        break;
    default:
    {
        position p = {.x=-1,.y=-1};
        return p;
    }
        break;
    }

}


/**
 * @brief Helper to convert an absolute position in the aquarium to a relative position with % on a specific view
*/
position get_relative_position_in_view(int absolute_x, int absolute_y, view* v) {
    position pos = {.x = -1, .y=-1};
    
    int new_origin_x = absolute_x - v->x;

    int new_origin_y = absolute_y - (v->y - v->height);
    pos.x = (new_origin_x*100)/v->width;
    pos.y = (new_origin_y*100)/v->height;
    return pos;
}

position get_absolute_position_from_view_pos(int relative_x, int relative_y, view* v) {
    position pos = {.x = -1, .y=-1};

    pos.x = (relative_x*v->width)/100 + v->x;
    pos.y = (relative_y*v->height)/100 + v->y;


    return pos;
}