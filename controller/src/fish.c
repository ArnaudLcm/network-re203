#include "fish.h"
#include "aquarium.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "utils.h"
#include "semaphore.h"
#include "string.h"
#include "utils.h"
#include "logger.h"
#include "linked_list.h"
#include "view.h"

sem_t semaphore_fishs;

int get_method_identifier(char* method) {
    char* well_formated_method = calloc(sizeof(method), sizeof(method)+1);
    strcpy(well_formated_method, method);
    if(strcmp(well_formated_method, "RandomWayPoint") == 0) {
        return RandomWayPoint;
    }

    if(strcmp(well_formated_method, "Horizontal") == 0) {
        return Horizontal;
    }
    if(strcmp(well_formated_method, "Vertical") == 0) {
        return Vertical;
    }
    if(strcmp(well_formated_method, "Diagonal") == 0) {
        return Diagonal;
    }
    return -1;

}

void free_fishs() {
    node* current_fish = aqua->list_fishs->tail;

    while(current_fish != NULL && current_fish->previous != NULL) {
        current_fish = current_fish->previous;
        free(current_fish->next);
    }

    free(current_fish);
}

node* get_fish_by_id(char* id) {
    node* current_fish = aqua->list_fishs->head;

    while(current_fish != NULL) {
        if(strcmp(((fish*)current_fish->value)->name, id) == 0) {
            return current_fish;
        }
        current_fish = current_fish->next;
    }

    return NULL;
}

bool remove_fish(char* id) {
    node* nd_fs = get_fish_by_id(id);

    sem_wait(&semaphore_fishs);

    del_node(aqua->list_fishs, nd_fs);
    free(nd_fs->value);
    free(nd_fs);
    sem_post(&semaphore_fishs);
    return TRUE;

}

bool add_fish(char* id, int x, int y, int width, int height, char* method, int client_fd) {

    if(is_obj_out_of_box(x, y, width, height, 100, 100)) {
        return FALSE;
    }

    int method_id = get_method_identifier(method);

    if(method_id == -1) { // The method movement deplacement pattern doesn't exist.
        return FALSE;
    }

    node* nd_v = get_view_by_owner(client_fd);

    if(nd_v == NULL) {
        return FALSE;
    }

    sem_wait(&semaphore_fishs);
    node* nd_fs = get_fish_by_id(id);
    if(nd_fs != NULL) {
        sem_post(&semaphore_fishs);
        return FALSE;
    }
    node *n_fish = malloc(sizeof(node));
    n_fish->next = NULL;
    n_fish->previous = NULL;
    n_fish->value = malloc(sizeof(fish));
    fish* fish = n_fish->value;

    fish->height = height;
    fish->mobility = method_id;
    fish->width = width;
    fish->status = NOT_STARTED;
    fish->pos = get_absolute_position_from_view_pos(x, y, nd_v->value);
    fish->name = calloc(sizeof(char), strlen(id)+1);
    strcat(fish->name, id);

    add_node(aqua->list_fishs, n_fish);

    sem_post(&semaphore_fishs);
    return TRUE;
}