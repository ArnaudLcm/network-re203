#include "linked_list.h"
#include "stdlib.h"
#include "utils.h"
#include "logger.h"

/**
 * @brief Add a node the tail of a list
 * @note n->next is assumed to be null
*/
bool add_node(linked_list* l, node* n) {
    if(n == NULL) {
        return FALSE;
    }

    if(l->head == NULL) {
        l->head = n;
        l->tail = l->head;
    } else {
        l->tail->next = n;
        n->previous = l->tail;
        l->tail = n;
    }

    return TRUE;

}

bool del_node(linked_list* l, node* n) {
    if(n == NULL) {
        return FALSE;
    }


    if (n->previous)
    {
        n->previous->next = n->next;
    }
    else
    { // It's the head that we are trying to delete
        l->head = n->next;
    }

    if (n->next)
    {
        n->next->previous = n->previous;
    } else {
        l->tail = n->previous;
    }

    return FALSE;

}