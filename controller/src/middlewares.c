#include "middlewares.h"
#include "user.h"
#include "processer.h"
#include <stdlib.h>
#include <stdio.h>
#include "utils.h"
#include "logger.h"
#include <sys/time.h>


/**
 * @brief This middleware is placed before a controller method in order to verify that the requester is
 * registered. If not, cancel the request.
*/
int auth_required_middleware(int (*next) (fish_instruction *, int), int fd, fish_instruction* fi) {
    node* nd_usr = get_user_by_fd(fd);

    if(nd_usr == NULL) {
        send_error_response(fd);
        return FALSE;
    }

    // Update updated_at field from user

    struct timeval tv;

    gettimeofday(&tv, NULL);

    ((user*)nd_usr->value)->updated_at = tv.tv_sec;

    return next(fi, fd);
}