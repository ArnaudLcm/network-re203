#ifndef __PROCESSER_H__
#define __PROCESSER_H__

void send_partial(char* message, int fd);
void send_formatted_partial(int fd, char* format, ...);
void send_response(char* response, int fd);
void send_formatted_response(int fd, char *format, ...);
void send_error_response(int fd);
int request_processer(char *input, int client_fd, int server_error_code);

#endif // !__PROCESSER_H__

