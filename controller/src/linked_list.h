#ifndef __LINKED_LIST_H__
#define __LINKED_LIST_H__
#include "utils.h"

typedef struct node node;

struct node {
    node* previous;
    node* next;
    void* value;
};

typedef struct {
    node* head;
    node* tail;
} linked_list;

bool del_node(linked_list* l, node* n);
bool add_node(linked_list* l, node* n);



#endif