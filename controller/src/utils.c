#include "utils.h"
#include <stdlib.h>
#include <stdio.h>
#include <stdarg.h>
#include <string.h>
#include "fcntl.h"
#include "math.h"
#include "logger.h"
#include <errno.h>  

/**
 * @brief The tuple (x,y) of a view gives the position of the top left corner of an object (view, fish, ...).
 * With this info and depending on his size, this function determines wether the object is out of aquarium's bounds.
 * The origin of the Cartesian coordinate system is at the left bottom corner of the aquarium.
*/
bool is_obj_out_of_box(int x, int y, int width, int height, int aquarium_width, int aquarium_height) {

    if(x < 0) { // Out of left bound
        return TRUE;
    }

    if(y+height > aquarium_height) { // Out of top bound
        return TRUE;
    }


    if(y < 0) { // Out of bottom bound
        return TRUE;
    }

    if(x+width > aquarium_width) { // Out of right bound
        return TRUE;
    }


    return FALSE;

}


bool is_obj_out_square(int x, int y, int square_width, int square_height, int square_x, int square_y) {

    if(x < square_x) { // Out of left bound
        return TRUE;
    }

    if(y < square_y) { // Out of top bound
        return TRUE;
    }


    if(y > square_y + square_height) { // Out of bottom bound
        return TRUE;
    }

    if(x > square_x + square_width) { // Out of right bound
        return TRUE;
    }


    return FALSE;

    
}


/**
 * @brief Determine if 2 linear equations intersect in some point
 * @param a : slope of equation 1
 * @param b : intercept of equation 1
 * @param c : slope of equation 2
 * @param d : intercept of equation 2
*/
bool does_intersect(float a, float b, float c, float d) {
    return (a*b-c*d) != 0.0;
}

bool is_view_concerned_by_fish(position fishStartPos, position fishEndPos, position viewPos, int viewWidth, int viewHeight) {

    if(fishStartPos.x < viewPos.x && fishEndPos.x < viewPos.x) {
        return FALSE;
    }


    if(fishStartPos.x > (viewPos.x + viewWidth) && fishEndPos.x > (viewPos.x + viewHeight)) {
        return FALSE;
    }

    if(fishStartPos.y == fishEndPos.y) {
        if(fishStartPos.y >= viewPos.y && fishStartPos.y <= (viewPos.y + viewHeight)) {
            return TRUE;
        } else {
            return FALSE;
        }


    }

    float a;
    float aNewRef;
    if(fishEndPos.x - fishStartPos.x == 0) {
        a = INFINITY;
    } else {
        a = (fishEndPos.y-fishStartPos.y)/(fishEndPos.x - fishStartPos.x); // Slope
    }

    if(fishEndPos.y - fishStartPos.y == 0) {
        aNewRef = INFINITY;

    } else {
        aNewRef = (fishEndPos.x - fishStartPos.x)/(fishEndPos.y-fishStartPos.y); // Slope

    }



    float b = a * fishStartPos.x - fishStartPos.y; //Intercept

    float bNewRef = aNewRef * fishStartPos.y - fishStartPos.x; //Intercept

    if(does_intersect(a,b, 0, viewPos.y)) {
        return TRUE;
    }

    if(does_intersect(a, b, 0, viewPos.y + viewHeight)) {
        return TRUE;
    }


    if(does_intersect(aNewRef,bNewRef, viewPos.y, 0)) {
        return TRUE;
    }

    if(does_intersect(aNewRef, bNewRef, viewPos.y + viewHeight, 0)) {
        return TRUE;
    }

    return FALSE;
}




int get_int_size(int a) {
    int i = 0;

    if(a == 0) {
        return 1;
    }

    while(a != 0) {
        a /= 10;

        i++;
    }

    return i;

}
char* itoa(int a) {
    char* cr = calloc(sizeof(char), get_int_size(a)+1);
    sprintf(cr, "%d", a);
    return cr;
}

void str_multiple_cpy(char* str, int nbr_cpy, ...) {
    va_list args;

    va_start(args, nbr_cpy);

    for (size_t i = 0; i < nbr_cpy; i++)
    {
        char* arg = va_arg(args, char*);

        strcat(str, arg);
    }
    va_end(args);

}

int is_valid_fd(int fd)
{
    return fcntl(fd, F_GETFL) != -1 || errno != EBADF;
}