#ifndef __CONFIG_H__
#define __CONFIG_H__

extern int LOGGING_LEVEL;
extern int FILES_LOGGING;
extern int PORT;
extern int MAX_THREAD_POOL_SIZE;
extern int INIT_THREAD_POOL_SIZE;
extern int TIMEOUT_USER;
extern double TOP_THRESHOLD_POOL;
extern double BOTTOM_THRESHOLD_POOL; 
extern int UPDATE_FISHS_POSITIONS;

void init();

#endif // __CONFIG_H__