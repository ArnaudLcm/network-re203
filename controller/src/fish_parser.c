#include "fish_parser.h"
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include "logger.h"
#include <stdarg.h>
#include <ctype.h>

#define NUMBER_OF_OPERATORS 3

#define NUMBER_OF_SKIPPING_CHARS 3


char* operator_list[NUMBER_OF_OPERATORS];

int char_skip[NUMBER_OF_SKIPPING_CHARS];


void init_method_list() {
    operator_list[0] = "x";
    operator_list[1] = ",";
    operator_list[2] = "+";
    char_skip[0] = '\n';
    char_skip[1] = '\t';
    char_skip[2] = '\r';

}


void free_instruction_fish(fish_instruction* fi) {
    for (size_t i = 0; i < fi->args_size; i++)
    {
        free(fi->args[i].value);
    }
    free(fi->args);
    free(fi);
    
}

/**
 * @brief Helper to check if a list of tokens are valid depending on their name value.
*/
int is_token_list_valid(fish_instruction* fi, int nbr_tokens, ...) {
    va_list args;

    va_start(args, nbr_tokens);

    if(nbr_tokens != fi->args_size) {
        return 0;
    }

    for (size_t i = 0; i < nbr_tokens; i++)
    {
        int token_type = va_arg(args, int);
        if(fi->args[i].name != token_type) {
            return 0;
        }
    }

    return 1;
    
}

/**
 * @brief Check if a string or a char is a number
*/
int isNumber(char* s)
{
    for (int i = 0; s[i]!= '\0'; i++)
    {
        if (isdigit(s[i]) == 0)
              return 0;
        
    }
    return 1;
}


char* skip_spaces(char* buffer) {
    while(*buffer == ' ' && *buffer != '\0') {

        buffer++;
    }
    return buffer;

}

char* format_str(char* str, int str_size) {
    for (size_t i = 0; i < str_size; i++)
    {
        str[i] = 0;
    }

    return str;
}

size_t next_space(char* buffer) {
    size_t i = 1;
    while(buffer[i] != ' ' && buffer[i] != '\0') {
        i++;
    }

    return i;
}

int find_op_symbol_pos(char* buffer, int size) {
    for (size_t i = 0; i < size; i++)
    {
        for (size_t j = 0; j < NUMBER_OF_OPERATORS; j++)
        {
            if(buffer[i] == *operator_list[j]) {
                return i;
            }
        }
           
    }

    return size;
    
}

void add_new_arg(fish_instruction* fi, char* arg, int size) {
        char* tk_str  = format_str(malloc(sizeof(char)*(size+1)), size+1);
        int offset = 0;
        for (size_t i = 0; i < size; i++)
        {
            int to_skip = 0;

            for (size_t j = 0; j < NUMBER_OF_SKIPPING_CHARS; j++)
            {
                if(arg[i] == char_skip[j]) {
                    to_skip = 1;
                    offset += 1;
                }
            }

            if(to_skip == 0) {
                tk_str[i-offset] = arg[i];
            }
            
        }

        if(strlen(tk_str) == 0) {
            return;
        }
        

        fi->args[fi->args_size].name = isNumber(tk_str) ? NBR : STR;
        if(isNumber(tk_str)) {
            int* vl = malloc(sizeof(int));
            *vl = atoi(tk_str);
            fi->args[fi->args_size].name = NBR;
            fi->args[fi->args_size].value = vl;

        } else {
            fi->args[fi->args_size].name = STR;

            for (size_t i = 0; i < NUMBER_OF_OPERATORS; i++)
            {
                if(strcmp(tk_str, operator_list[i]) == 0) {
                    fi->args[fi->args_size].name = OP;
                }
            }
            
            fi->args[fi->args_size].value = tk_str;
        }
        fi->args_size += 1;

}

char* find_op(fish_instruction* fi, char* buffer, int size) {

    int next_op_pos = find_op_symbol_pos(buffer, size);

    if(next_op_pos == size) {
        add_new_arg(fi, buffer, size);
        buffer = buffer+size;
        return buffer;
    }


    while(next_op_pos != size) {
        
        add_new_arg(fi, buffer, next_op_pos);
        buffer = buffer + next_op_pos;
        size = size - next_op_pos;

        add_new_arg(fi, buffer, 1);
        buffer = buffer+1;
        size -= 1;
        next_op_pos = find_op_symbol_pos(buffer, size);

    }
    add_new_arg(fi, buffer, next_op_pos);

    buffer = buffer+next_op_pos;

    return buffer;
}


/**
 * @brief The purpose of this function is to decompose a string into an array of tokens.
 * Each token is composed by 2 value : his type (number, string, ...) and a value
*/
int lexxer(fish_instruction* fi, char* buffer) {
    if(*buffer == '\0' || *buffer == '\n') {
        return 0;
    }
    skip_spaces(buffer);

    while(*buffer != '\0') {
        

        size_t ns = next_space(buffer);
        int diff = (ns == 0) ? strlen(buffer) : ns;

        buffer = find_op(fi, buffer, diff);


        buffer = skip_spaces(buffer);


    }

    return 0;
}