#ifndef __UTILS_H__
#define __UTILS_H__

#define TRUE 1
#define FALSE 0

typedef int bool;

typedef struct {
    int x;
    int y;
} position;


bool is_obj_out_of_box(int x, int y, int width, int height, int aquarium_width, int aquarium_height);

bool is_obj_out_square(int x, int y, int square_width, int square_height, int square_x, int square_y);

bool is_view_concerned_by_fish(position fishStartPos, position fishEndPos, position viewPos, int viewWidth, int viewHeight);
char* itoa(int a);

int get_int_size(int a);

void str_multiple_cpy(char* str, int nbr_cpy, ...);

int is_valid_fd(int fd);

#endif // __UTILS_H__