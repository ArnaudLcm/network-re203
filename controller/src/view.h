#ifndef __VIEW_H__
#define __VIEW_H__
#include "utils.h"
#include "semaphore.h"
#include "linked_list.h"

typedef struct view view;


enum owner_state {
    NO_OWNER=-1
};

struct view {
    char* id;
    int owner;
    int x;
    int y;
    int width;
    int height;
};

extern sem_t semaphore_views;


bool take_view(char *id, int new_owner);

node* find_available_view();

node* get_view_by_id(char *id);

node* get_view_by_owner(int fd);


bool does_view_already_exist(char *id);

bool remove_view_by_id(char *id);

bool remove_owner(int fd);

void free_views();

#endif // !__VIEW_H__