#include "controllers.h"
#include "fish_parser.h"
#include <stdio.h>
#include <stdlib.h>
#include "processer.h"
#include "string.h"
#include "aquarium.h"
#include "logger.h"
#include <sys/time.h>
#include "fish.h"
#include "linked_list.h"

int add_fish_controller(fish_instruction *fi, int client_fd)
{
    if (add_fish((char *)fi->args[1].value, *(int *)fi->args[3].value, *(int *)fi->args[5].value, *(int *)fi->args[7].value, *(int *)fi->args[9].value, (char *)fi->args[11].value, client_fd))
    {
        send_response("OK", client_fd);
    }
    else
    {
        send_error_response(client_fd);
    }
    return EXIT_SUCCESS;
}

int del_fish_controller(fish_instruction *fi, int client_fd)
{

    if (remove_fish(fi->args[1].value))
    {
        send_response("OK", client_fd);
    }
    else
    {
        send_error_response(client_fd);
    }

    return EXIT_SUCCESS;
}

int start_fish_controller(fish_instruction *fi, int client_fd)
{
    node* nd_f = get_fish_by_id(fi->args[1].value);
    if(nd_f == NULL) {
        send_error_response(client_fd); // Fish doesn't exist
        return EXIT_SUCCESS;
    }

    ((fish*)nd_f->value)->status = STARTED;

    send_response("OK", client_fd);

    return EXIT_SUCCESS;
}

int ping_controller(fish_instruction *fi, int client_fd)
{

    node* usr = get_user_by_fd(client_fd);
    sem_wait(&semaphore_users);
    // Update updated_at of user
    struct timeval tv;
    gettimeofday(&tv, NULL);
    ((user*)usr->value)->updated_at = tv.tv_sec;
    sem_post(&semaphore_users);

    char* response;
    asprintf(&response, "pong %d\n", *(int*)fi->args[1].value);
    send_response(response, client_fd);
    return EXIT_SUCCESS;
}

int log_controller(fish_instruction *fi, int client_fd)
{
    node* usr = get_user_by_fd(client_fd);
    send_response("bye\n", client_fd);
    if(usr != NULL) {
        remove_user_safely(client_fd);
    }

    return EXIT_SUCCESS;
}

int ls_controller(fish_instruction *fi, int client_fd)
{
    send_response("OK", client_fd);

    return EXIT_SUCCESS;
}

int get_fishes_controller(fish_instruction *fi, int client_fd)
{
    if (aqua == NULL)
    { // No aquarium is currently loaded
        send_error_response(client_fd);
        return EXIT_FAILURE;
    }

    node* nd_v = get_view_by_owner(client_fd);

    node* nd_u = get_user_by_fd(client_fd);

    if (nd_v == NULL)
    { // The user doesn't have any associated view
        send_error_response(client_fd);
        return EXIT_FAILURE;
    }

    char *response = calloc(sizeof(char), REQUEST_BUFFER_SIZE);
    strcat(response, "list"); // Starting to format response

    node_user_instruction *current_instruction = pop_instruction(nd_u->value);

    while (current_instruction != NULL)
    {
        position relative_pos = get_relative_position_in_view(current_instruction->value->x, current_instruction->value->y, nd_v->value);

        char *formatted_x = itoa(relative_pos.x);
        char *formatted_y = itoa(relative_pos.y);
        char *formatted_width = itoa(current_instruction->value->width);
        char *formatted_height = itoa(current_instruction->value->height);
        char *formatted_speed = itoa(current_instruction->value->speed);

        str_multiple_cpy(response, 13,
                         " [",
                         current_instruction->value->fish_name,
                         " at ",
                         formatted_x,
                         "x",
                         formatted_y,
                         ",",
                         formatted_width,
                         "x",
                         formatted_height,
                         ",",
                         formatted_speed,
                         "]");
        free_node_instruction(current_instruction);

        current_instruction = pop_instruction(nd_u->value);
    }
    strcat(response, "\n");
    send_response(response, client_fd);
    return EXIT_SUCCESS;
}

int get_fishes_continuously_controller(fish_instruction *fi, int client_fd)
{
    node* nd_usr = get_user_by_fd(client_fd);
    ((user*)nd_usr->value)->getting_fishes_continuously = TRUE;
    send_formatted_response(client_fd, "OK\n");

    return EXIT_SUCCESS;
}

int hello_controller(char *id, int client_fd)
{
    if (aqua == NULL)
    {

        send_response("no greeting", client_fd);
        return EXIT_SUCCESS;
    }

    node* nd_usr = get_user_by_fd(client_fd);
    if(nd_usr == NULL) { // The user has not been registered yet
        add_user(client_fd);
    }

    node* nd_view =  get_view_by_owner(client_fd);

    if (id == NULL)
    {
        node* available_node = find_available_view();

        if (available_node == NULL)
        {
            send_response("no greeting", client_fd);
            return EXIT_FAILURE;
        }

        if(nd_view != NULL) { // The user already owned a view
            ((view*)nd_view->value)->owner = NO_OWNER; // Unlink the view
        }

        take_view(((view*)available_node->value)->id, client_fd);

        char* response;
        asprintf(&response, "greeting %s\n", ((view*)available_node->value)->id);
        send_response(response, client_fd);

        return EXIT_SUCCESS;
    }

    if (take_view(id, client_fd)) // Case where the user provided a wanted ID
    {

        if(nd_view != NULL) { // The user already owned a view
            ((view*)nd_view->value)->owner = NO_OWNER; // Unlink the view
        }

        send_formatted_response(client_fd, "greeting %s", id);

        return EXIT_SUCCESS;
    }
    else
    {
        node* available_node = find_available_view();

        if(nd_view != NULL) { // The user already owned a view
            ((view*)nd_view->value)->owner = NO_OWNER; // Unlink the view
        }

        take_view(((view*)available_node->value)->id, client_fd);

        send_formatted_response(client_fd, "greeting %s", ((view*)available_node->value)->id);

        return EXIT_SUCCESS;
    }

    return EXIT_SUCCESS;
}