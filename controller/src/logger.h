#ifndef __LOGGER_H__
#define __LOGGER_H__

void log_success(char *s, ...);
void log_info(char *s, ...);
void log_warning(char *s, ...);
void log_error(char *s, ...);
void log_debug(char *s, ...);

#endif // __LOGGER_H__