#ifndef __USER__H
#define __USER__H
#include "fish.h"
#include "utils.h"
#include "semaphore.h"
#include "linked_list.h"


extern sem_t semaphore_users;


typedef struct {
    char* fish_name;
    int x;
    int y;
    int width;
    int height;
    int speed;
} user_instruction;

typedef struct node_user_instruction node_user_instruction;

struct node_user_instruction {
    user_instruction* value;
    node_user_instruction* next;
    node_user_instruction* previous;
};

typedef struct {
    int fd;
    unsigned long updated_at;
    node_user_instruction* instructions;
    bool getting_fishes_continuously; // Boolean to store if this client is executing getFishesContinuously command
} user;



bool push_instruction(user* usr, user_instruction* istr);

node_user_instruction* pop_instruction(user* usr);

void free_users();


bool add_user(int fd);

bool is_user_timedout(node* nd_user);

bool remove_user_safely(int fd);

node* get_user_by_fd(int fd);


node* get_safely_user(int fd);

bool remove_user(int fd);

void free_node_instruction(node_user_instruction* nd_u);
void free_user_instructions(user* user);


#endif // !__USER__H