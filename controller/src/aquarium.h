#ifndef __AQUARIUM_H__
#define __AQUARIUM_H__
#include "user.h"
#include "view.h"
#include "utils.h"
#include "fish.h"
#include "linked_list.h"


typedef struct {
    int width;
    int height;
    linked_list* list_view;
    linked_list* list_users;
    linked_list* list_fishs;
} aquarium;

extern aquarium *aqua;

void start_fish_manager();

bool save(char* aquarium_name);

bool show_aquarium();

bool is_aquarium_load();

bool load(char* aquarium_name);

void updateContinuousListeners();

void free_aquarium();

#endif // !__AQUARIUM_H__
