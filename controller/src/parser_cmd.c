#include "parser_cmd.h"
#include "logger.h"
#include "fish_parser.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "aquarium.h"
#include "utils.h"
#include "view.h"
#include "linked_list.h"


int handle_load_cmd(fish_instruction* fi) {

    load(fi->args[1].value);
    free_instruction_fish(fi);

    return TRUE;

}

int handle_show_cmd(fish_instruction* fi) {
    if(!show_aquarium()) {
        log_warning("No aquarium is currently loaded.");
    }
    free_instruction_fish(fi);

    return TRUE;
}


void handle_state_cmd(fish_instruction* fi) {
    if(aqua == NULL) {
        log_warning("No aquarium is currently loaded");
        return;
    }

    log_info("---- USERS ----");
    node* current_node_user = aqua->list_users->head;
    if(current_node_user == NULL) {
        log_info("No users");
    } else {
        int i = 0;

        while(current_node_user != NULL) {
            log_info("No: %d \t  Last update : %ld \t fd: %d",i, ((user*)current_node_user->value)->updated_at, (((user*)current_node_user->value)->fd));
            current_node_user = current_node_user->next;
            i++;
        }
    }
    log_info("---- VIEWS ----");
    node* current_view = aqua->list_view->head;
    if(current_view == NULL) {
        log_info("No views");
    } else {
        while(current_view != NULL) {
            log_info("ID: %s \t  Owned: %d", ((view*)current_view->value)->id, ((view*)current_view->value)->owner);
            current_view = current_view->next;
        }
    }
    log_info("---- FISHS ----");
    node* current_fish = aqua->list_fishs->head;
    if(current_fish == NULL) {
        log_info("No fish");
    } else {
        while(current_fish != NULL) {
            log_info("ID: %s \t Deplacement pattern id : %d \t Position: %dx%d \t Taille: %dx%d",
                ((fish*)current_fish->value)->name,
                ((fish*)current_fish->value)->mobility,
                ((fish*)current_fish->value)->pos.x, ((fish*)current_fish->value)->pos.y,
                ((fish*)current_fish->value)->width, ((fish*)current_fish->value)->height
            );
            current_fish = current_fish->next;
        }
    }
}


int handle_add_cmd(fish_instruction* fi) {
    if(!is_aquarium_load()) {
        log_warning("No aquarium is currently loaded.");
        return FALSE;
    }
    if(does_view_already_exist(fi->args[2].value)) {
        log_warning("%s is already used in the config.", (char*)fi->args[2].value);
        return FALSE;
    }



    if(!is_obj_out_of_box(*(int*)fi->args[3].value, *(int*)fi->args[5].value, *(int*)fi->args[7].value, *(int*)fi->args[9].value, aqua->width, aqua->height)) {
        node* n = malloc(sizeof(node));
        n->next = NULL;
        n->previous = NULL;
        n->value = malloc(sizeof(view));

        view* new_view = n->value;

        new_view->id = calloc(sizeof(char), strlen((char*)fi->args[2].value)+1);
        strcpy(new_view->id, (char*)fi->args[2].value);
        new_view->x = *(int*)fi->args[3].value;
        new_view->y = *(int*)fi->args[5].value;
        new_view->width = *(int*)fi->args[7].value;
        new_view->height = *(int*)fi->args[9].value;
        new_view->owner = NO_OWNER;

        add_node(aqua->list_view, n);
        log_success("View %s has been succesffuly added.", (char*)fi->args[2].value);

        return TRUE;
    }
    log_warning("This view is out of bounds.");
    free_instruction_fish(fi);

    return FALSE;

}

int handle_del_cmd(fish_instruction* fi) {
    remove_view_by_id(fi->args[2].value);
    free_instruction_fish(fi);
    return TRUE;
}

int handle_save_cmd(fish_instruction* fi) {
    save(fi->args[1].value);
    free_instruction_fish(fi);
    return TRUE;

}

void invalid_cmd_format(char* format) {
    log_warning("Please refect the format : %s", format);
}


/**
 * @brief The purpose of this function is parse the input from the command into tokens and then root the cmd to the appropriated
 * handler.
 * @example If the command is : show aquarium , it will be transformed into 2 tokens. The first token is always the cmd
 * name so depending on this value, it will be rooted differently. In this case, it's the show cmd handler which will be
 * triggered.
*/
int handle_cmd(char* cmd) {

    fish_instruction* fi = malloc(sizeof(fish_instruction));
    fi->args = malloc(sizeof(token)*NUMBER_OF_ARGS);
    fi->args_size = 0;
    lexxer(fi, cmd);

    if(fi->args_size == 0 || fi->args[0].name != STR) {
        log_warning("Invalid command.");
        return FALSE;
    }


    // Rooting cmd
    if(strcmp(fi->args[0].value, "load") == 0) {
        if(fi->args_size == 2 && is_token_list_valid(fi, 2, STR, STR)) {
            return handle_load_cmd(fi);
        } else {
            invalid_cmd_format("load <aquarium-name>");
            free_instruction_fish(fi);

            return FALSE;
        }

    } else if(strcmp(fi->args[0].value, "show") == 0) {
        
        if(fi->args_size == 2 && is_token_list_valid(fi, 2, STR, STR)) {
            return handle_show_cmd(fi);
        } else {
            invalid_cmd_format("show aquarium");
            free_instruction_fish(fi);

            return FALSE;
        }

    } else if(strcmp(fi->args[0].value, "add") == 0) {
        if(fi->args_size == 10 && is_token_list_valid(fi, 10, STR, STR, STR, NBR, OP, NBR, OP, NBR, OP, NBR)) {
            return handle_add_cmd(fi);
        } else {
            invalid_cmd_format("add view ...args");
            free_instruction_fish(fi);

            return FALSE;
        }

    } else if(strcmp(fi->args[0].value, "del") == 0) {
        if(fi->args_size == 3 && is_token_list_valid(fi, 3, STR, STR, STR)) {
            return handle_del_cmd(fi);
        } else {
            invalid_cmd_format("delete view <view-id>");
            free_instruction_fish(fi);

            return FALSE;
        }

    } else if(strcmp(fi->args[0].value, "save") == 0) {
        if(fi->args_size == 2 && is_token_list_valid(fi, 2, STR, STR)) {
            return handle_save_cmd(fi);
        } else {
            invalid_cmd_format("save <aquarium-name>");
            free_instruction_fish(fi);

            return FALSE;
        }
    } else if(strcmp(fi->args[0].value, "state") == 0) {
        if(fi->args_size == 1 && is_token_list_valid(fi, 1, STR)) {
            handle_state_cmd(fi);
            return TRUE;
        }
    } else if(strcmp(fi->args[0].value, "quit") == 0) {

        free_instruction_fish(fi);
        free_aquarium();

        exit(0);
        return TRUE;
    }

    free_instruction_fish(fi);
    log_warning("Unknown command.");
    return FALSE;
}