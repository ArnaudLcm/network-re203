#include "test_fishparser.h"
#include "tests.h"
#include "fish_parser.h"
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "logger.h"
void test_fish_parser_batch() {
    test_lexxer_1();
    test_lexxer_2();
    test_lexxer_3();
}

void test_lexxer_1() {
    fish_instruction* fi = malloc(sizeof(fish_instruction));
    fi->args = malloc(sizeof(token)*NUMBER_OF_ARGS);
    fi->args_size = 0;
    char* buffer = "ping t 12345 12";

    init_method_list();

    lexxer(fi, buffer);
    ASSERT_TRUE(strcmp(fi->args[0].value, "ping") == 0);
    ASSERT_TRUE(fi->args[0].name == STR);


    ASSERT_TRUE(strcmp(fi->args[1].value, "t") == 0);
    ASSERT_TRUE(fi->args[0].name == STR);

    ASSERT_TRUE(*(int*)fi->args[2].value == 12345);
    ASSERT_TRUE(fi->args[2].name == NBR);

    ASSERT_TRUE(*(int*)fi->args[3].value == 12);
    ASSERT_TRUE(fi->args[2].name == NBR);

    free_instruction_fish(fi);
    
}

void test_lexxer_2() {
    fish_instruction* fi = malloc(sizeof(fish_instruction));
    fi->args = malloc(sizeof(token)*NUMBER_OF_ARGS);
    fi->args_size = 0;
    char* buffer = "pxing test";

    init_method_list();

    lexxer(fi, buffer);

    ASSERT_TRUE(strcmp(fi->args[0].value, "p") == 0);
    ASSERT_TRUE(fi->args[0].name == STR);


    ASSERT_TRUE(strcmp(fi->args[1].value, "x") == 0);
    ASSERT_TRUE(fi->args[1].name == OP);

    ASSERT_TRUE(strcmp(fi->args[2].value, "ing") == 0);
    ASSERT_TRUE(fi->args[2].name == STR);
    


    ASSERT_TRUE(strcmp(fi->args[3].value, "test") == 0);
    ASSERT_TRUE(fi->args[3].name == STR);


    
    free_instruction_fish(fi);
}


void test_lexxer_3() {
    fish_instruction* fi = malloc(sizeof(fish_instruction));
    fi->args = malloc(sizeof(token)*NUMBER_OF_ARGS);
    fi->args_size = 0;
    char* buffer = "pi\n\tng\n";

    init_method_list();

    lexxer(fi, buffer);


    ASSERT_TRUE(strcmp(fi->args[0].value, "ping") == 0);
    ASSERT_TRUE(fi->args[0].name == STR);

    free_instruction_fish(fi);
}