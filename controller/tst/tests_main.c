#include <stdlib.h>
#include <stdio.h>

#include "config.h"
#include "tests.h"
#include "test_example.h"
#include "test_fishparser.h"
#include "test_utils.h"
#include "test_movements.h"
#include "logger.h"

int main(int argc, char *argv[])
{
    init();

    new_test_batch("test_fish_parser_batch");
	test_fish_parser_batch();
	end_test_batch();

	new_test_batch("test_utils_batch");
	test_utils_batch();
	end_test_batch();


	new_test_batch("test_movements");
	test_movements_batch();
	end_test_batch();

    return EXIT_SUCCESS;
}