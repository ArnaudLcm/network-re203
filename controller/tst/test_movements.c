#include "test_movements.h"
#include "movements.h"
#include <stdlib.h>
#include <stdio.h>
#include "tests.h"
#include "logger.h"

void test_relative_pos_view() {
    view* v = malloc(sizeof(view));
    v->x = 20;
    v->y = 40;
    v->width = 40;
    v->height = 20;

    position p = get_relative_position_in_view(40, 30, v);


    ASSERT_TRUE(p.x == 50);
    ASSERT_TRUE(p.y == 50);

    free(v);
}

void test_get_absolute_position_from_view_pos() {
    view* v = malloc(sizeof(view));
    v->x = 20;
    v->y = 40;
    v->width = 40;
    v->height = 20;

    position p = get_absolute_position_from_view_pos(50, 50, v);


    ASSERT_TRUE(p.x == 40);
    ASSERT_TRUE(p.y == 30);

    free(v);
}

void test_movements_batch() {
    test_relative_pos_view();
    test_get_absolute_position_from_view_pos();
}