#include "tests.h"
#include "test_utils.h"
#include "utils.h"
#include <stdlib.h>
#include <stdio.h>
#include "string.h"
#include "logger.h"
#include "view.h"
#include "movements.h"



void test_get_int_size() {

    int a = 0;

    int b = 1;

    int c = 22;

    int d = 1000;


    ASSERT_TRUE(get_int_size(a) == 1);
    ASSERT_TRUE(get_int_size(b) == 1);
    ASSERT_TRUE(get_int_size(c) == 2);
    ASSERT_TRUE(get_int_size(d) == 4);
}

void test_itoa() {
    ASSERT_TRUE(strcmp(itoa(1), "1") == 0);
    ASSERT_TRUE(strcmp(itoa(20), "20") == 0);
    ASSERT_TRUE(strcmp(itoa(101), "101") == 0);
}


void test_multiple_cpy() {
    char* test = calloc(sizeof(char), 12);

    str_multiple_cpy(test, 3, "hello", " ", "world");

    ASSERT_TRUE(strcmp(test, "hello world") == 0);

}


void test_is_view_concerned_1() {
    position startFish = {.x = 2, .y = 4};
    position endFish = {.x = 20, .y = 26};

    position viewPos = {.x = 6, .y = 4};

    int viewWidth = 6;
    int viewHeight = 14;

    ASSERT_TRUE(is_view_concerned_by_fish(startFish, endFish, viewPos, viewWidth, viewHeight));
}


void test_is_view_concerned_2() {
    position startFish = {.x = 7, .y = 1};
    position endFish = {.x = 7, .y = 20};

    position viewPos = {.x = 6, .y = 4};

    int viewWidth = 6;
    int viewHeight = 14;

    ASSERT_TRUE(is_view_concerned_by_fish(startFish, endFish, viewPos, viewWidth, viewHeight));
}

void test_is_view_concerned_3() {
    position startFish = {.x = 2, .y = 8};
    position endFish = {.x = 13, .y = 8};

    position viewPos = {.x = 6, .y = 4};

    int viewWidth = 6;
    int viewHeight = 14;

    ASSERT_TRUE(is_view_concerned_by_fish(startFish, endFish, viewPos, viewWidth, viewHeight));
}

void test_is_view_concerned_4() {
    position startFish = {.x = 2, .y = 8};
    position endFish = {.x = 5, .y = 10};

    position viewPos = {.x = 6, .y = 4};

    int viewWidth = 6;
    int viewHeight = 14;

    ASSERT_TRUE(!is_view_concerned_by_fish(startFish, endFish, viewPos, viewWidth, viewHeight));
}

void test_is_view_concerned_5() {
    position startFish = {.x = 1, .y = 2};
    position endFish = {.x = 3, .y = 6};

    position viewPos = {.x = 3, .y = 2};

    int viewWidth = 4;
    int viewHeight = 3;

    ASSERT_TRUE(!is_view_concerned_by_fish(startFish, endFish, viewPos, viewWidth, viewHeight));
}


void test_is_view_concerned_6() {
    position startFish = {.x = 1, .y = 3};
    position endFish = {.x = 8, .y = 4};

    position viewPos = {.x = 3, .y = 2};

    int viewWidth = 4;
    int viewHeight = 3;

    ASSERT_TRUE(is_view_concerned_by_fish(startFish, endFish, viewPos, viewWidth, viewHeight));
}



void test_utils_batch() {
    test_get_int_size();
    test_itoa();
    test_multiple_cpy();
    test_is_view_concerned_1();
    test_is_view_concerned_2();
    test_is_view_concerned_3();
    test_is_view_concerned_4();
    test_is_view_concerned_5();
    test_is_view_concerned_6();
}