MAKEFLAGS += --no-print-directory

controller:
	@cd controller && make

view:
	@cd view && make

.PHONY: controller view