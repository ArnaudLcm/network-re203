#[macro_use(lazy_static)]
extern crate lazy_static;

mod utils;
use crate::utils::{ ConfigParser, get_config };

mod client;
use crate::client::Client;

mod prelude {
    pub use crate::utils::logger::*;
}

lazy_static! {
    static ref CONFIG: ConfigParser = get_config();
}

fn main() {
    Client::new().run();
}