use crate::log_debug;

pub enum Token {
    String(String),
    Number(i32),
    Cross, // The 'x' inside 100x100
    ListKeyWord,
    ListItem(Vec<Token>),
}

pub struct Tokenizer {
    pub tokens: Vec<Token>,
}

impl Tokenizer {
    pub fn new() -> Tokenizer {
        return Tokenizer { tokens: Vec::new() };
    }

    /**
     * @brief The purpose of this fn is to turn a string into a list of tokens.
     * @note Maybe implemeting a derivation tree would be better than this match hell but in the current state of
     * the applicative protocole, it's for sure overkill
     */
    pub fn parse(&mut self, mut buffer: &str) {
        buffer = buffer.trim_matches(|c| (c == ' ' || c == '\t' || c == '\n' || c == ','));
        if buffer.starts_with("list") {
            self.tokens.push(Token::ListKeyWord);
            let mut item_list = Self::parse_list(&buffer[4..]);
            self.tokens.append(&mut item_list);
        } else {
            let mut exp = Self::parse_exp(buffer);
            self.tokens.append(&mut exp);
        }
    }

    fn parse_list(buffer: &str) -> Vec<Token> {
        let mut result_vec = Vec::new();

        buffer
            .split(|c| c == '[')
            .filter(|item| *item != "" && *item != "\t" && *item != " " && *item != "\n")
            .for_each(|t| {
                if t.contains(']') {
                    result_vec.push(
                        Token::ListItem(
                            Self::parse_exp(
                                t
                                .trim_matches(|c| (c == ' ' || c == '\t' || c == '\n' || c == ',' || c == '\0'))
                                .trim_matches(|c| (c == ']'))
                                .trim_matches(|c| (c == ' ' || c == '\t' || c == '\n' || c == ',' || c == '\0'))
                            )
                        )
                    );
                }
            });

        return result_vec;
    }

    fn parse_exp(buffer: &str) -> Vec<Token> {
        let mut result_vec = Vec::new();

        buffer
            .split(|c| c == ' ' || c == ',')
            .filter(|item| *item != "" && *item != "\t" && *item != " ")
            .map(|item| {
                return match item.trim_matches(|c| (c == ']' )).parse::<i32>() {
                    Ok(r) => Token::Number(r),
                    Err(_) => {
                        Token::String(
                            item.trim_matches(|c| (c == '\t' || c == '\n' || c == ',')).to_string()
                        )
                    }
                };
            })
            .for_each(|t| {
                match t {
                    Token::String(r) => {
                        let vec: Vec<&str> = r.split(|c| c == 'x').collect();
                        match vec.len() {
                            2 => {
                                match (vec[0].parse::<i32>(), vec[1].parse::<i32>()) {
                                    (Ok(n1), Ok(n2)) => {
                                        result_vec.push(Token::Number(n1));
                                        result_vec.push(Token::Cross);
                                        result_vec.push(Token::Number(n2));
                                    }
                                    _ => {
                                        result_vec.push(Token::String(r));
                                    }
                                }
                            }
                            1 =>
                                match vec[0].parse() {
                                    Ok(result) => result_vec.push(Token::Number(result)),
                                    Err(_) => result_vec.push(Token::String(r)),
                                }
                            0 => (), // Should not be triggered,
                            _ => result_vec.push(Token::String(r))
                        }
                    }
                    r => result_vec.push(r),
                }
            });

        return result_vec;
    }
}