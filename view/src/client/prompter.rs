use std::io;

use super::tokenizer::{Tokenizer};



pub struct PromptHandler {
    called_exit: bool,
}

/**
 * @brief The purpose of this class is to handle and parse inputs in the prompt for the user
 * @example By tapping status, the class will recognises that the wanted cmd is status and a specific
 * handler for this cmd will be triggered.
 */
impl PromptHandler {
    pub fn new() -> PromptHandler {
        return PromptHandler { called_exit: false };
    }

    /**
     * @brief The purpose of this function is to handle every inputs for the user and parse it
     */
    pub fn handle_input(&mut self, cmd_rooter: impl Fn(&mut PromptHandler, Tokenizer) -> ()) {
        let mut buffer_input = String::new();

        while !self.called_exit {
            io::stdin()
                .read_line(&mut buffer_input)
                .expect("An error occured while trying to read the line.");


            let mut tokenizer = Tokenizer::new();
            tokenizer.parse(&buffer_input);
            cmd_rooter(self, tokenizer);

            buffer_input = String::new();
        }
    }

    pub fn ask_exit(&mut self) {
        self.called_exit = true;
    }
}