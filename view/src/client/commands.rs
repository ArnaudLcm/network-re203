use std::{ sync::Arc, time::Instant };

use color_print::cprintln;
use super::{ PromptHandler, Client, tokenizer::{ Tokenizer, Token } };
use crate::{ prelude::*, client::client::{Instruction, Fish} };

impl Client {
    pub fn welcome_message() {
        cprintln!("<blue, bold>Welcome to Network Aquarium !</blue, bold>");
        Self::help_message();
    }

    pub fn handle_cmd_status(&self) {
        let stream = Arc::clone(&self.stream);
        let stream_mutex = stream.lock().unwrap();
        match &*stream_mutex {
            Some(_) => log_info!("You are connected"),
            None => log_error!("You are not connected to the server"),
        }
    }

    pub fn handle_cmd_connect(&self) {
        let stream = Arc::clone(&self.stream);
        let stream_mutex = stream.lock().unwrap();

        match &*stream_mutex {
            None => {
                drop(stream_mutex);
                self.connect();
            }
            Some(_) => log_info!("You are already connected to server"),
        }
    }

    pub fn handle_cmd_login(&self) {
        let view_id = Arc::clone(&self.view_id);
        let mut view_id_mutex = view_id.lock().unwrap();
        match &*view_id_mutex {
            Some(s) => {
                log_info!("Already connected as {s}");
                return;
            }
            None => (),
        }

        self.send_to_server(String::from("hello in as ") + &self.config.id);
        match self.wait_one_server_answer() {
            Ok(s) => {
                let mut tokenizer = Tokenizer::new();
                tokenizer.parse(&s);
                match &tokenizer.tokens[..] {
                    [Token::String(greeting), Token::String(id)] => {
                        if greeting.to_owned() == "greeting" {
                            *view_id_mutex = Option::Some(id.to_owned());
                            log_success!("Successfully connected as {id}");
                        } else {
                            log_error!("Incorrect server response : {s}");
                        }
                    }
                    _ => log_error!("Incorrect server response : {s}"),
                }
            }
            Err(r) => log_error!("Error while trying to log in to server : {r}"),
        }
    }

    pub fn handle_cmd_quit(&self, prompter: &mut PromptHandler) {
        log_info!("Exiting..");
        let stream = Arc::clone(&self.stream);
        let stream_mutex = stream.lock().unwrap();
        match &*stream_mutex {
            Some(_) => {
                drop(stream_mutex);
                self.send_to_server(String::from("log out"));
                self.consume_answer();
            }
            _ => drop(stream_mutex),
        }
        prompter.ask_exit();
    }

    pub fn help_message() {
        cprintln!("<bold>Commands list :</bold>");
        cprintln!("status \t \t Check if you are connected to a controller");
        cprintln!(
            "addFish NAME at XCOORDxYCOORD, WIDTHxHEIGHT, PATH_TYPE \t \t Add a fish with the specified parameters"
        );
    }

    pub fn handle_cmd_del_fish(&self, fish_name: &str) {
        self.send_to_server(String::from(format!("delFish {fish_name}")));
        match self.wait_one_server_answer() {
            Err(_) => (),
            Ok(answer) => log_info!("Server -> {answer}"),
        }
    }

    pub fn handle_cmd_start_fish(&self, fish_name: &str) {
        self.send_to_server(String::from(format!("startFish {fish_name}")));
        match self.wait_one_server_answer() {
            Err(_) => (),
            Ok(answer) => log_info!("Server -> {answer}"),
        }
    }

    pub fn handle_cmd_add_fish(
        &self,
        fish_name: &String,
        method: &String,
        x: &i32,
        y: &i32,
        w: &i32,
        h: &i32
    ) {
        self.send_to_server(format!("addFish {fish_name} at {x}x{y}, {w}x{h}, {method}"));
        match self.wait_one_server_answer() {
            Err(_) => (),
            Ok(answer) => {
                log_info!("Server -> {answer}");
            },
        }
    }

    pub fn handle_cmd_autosync(&self) {
        self.send_to_server(format!("getFishesContinuously"));
        match self.wait_one_server_answer() {
            Ok(s) => {
                if s == "NOK" {
                    log_error!("Server error while calling getFishesContinuously");
                    return;
                }   
            }
            Err(r) => log_error!("Error while calling getFishesContinuously : {r}"),
        }
        
        loop {
            if let Ok(answer) = self.wait_one_server_answer() {
                let fishes = self.fishes.clone();
                let mut fishes_mutex = fishes.lock().unwrap();
                let mut tokenizer = Tokenizer::new();
                tokenizer.parse(&answer);
                match &tokenizer.tokens[..] {
                    [Token::ListKeyWord, ..] => {
                        tokenizer.tokens[1..].iter().for_each(|t| { 
                            match t {
                                Token::ListItem(li) => {
                                    match &li[..] {
                                        [Token::String(name), Token::String(_), Token::Number(x), Token::Cross, Token::Number(y), Token::Number(w), Token::Cross, Token::Number(h), Token::Number(d)] => {
                                            match fishes_mutex.get_mut(name) {
                                                Some(f) => {
                                                    f.instructions.push(Instruction {
                                                        position_end: (*x as f64, *y as f64),
                                                        position_start: (-1.0, -1.0),
                                                        size_end: (*w, *h),
                                                        duration: *d,
                                                        start_date: None
                                                    });
                                            },
                                                None => {
                                                    let mut fish = Fish {
                                                        name: String::from(name),
                                                        position_current: (0.0, 0.0),
                                                        instructions: vec![]

                                                    };

                                                    fish.instructions.push(Instruction {
                                                        position_end: (*x as f64, *y as f64),
                                                        position_start: (-1.0, -1.0),
                                                        size_end: (*w, *h),
                                                        duration: *d,
                                                        start_date: None,
                                                    });


                                                    fishes_mutex.insert(String::from(name), fish);
                                                },
                                            }
                                            
                                        },
                                        _ => log_error!("Can't parse fish instruction list item"),
                                    }
                                }
                                _ => log_error!("Expected a list item but found something else"),
                            }
                        });
                        log_debug!("server sent {answer}");
                        self.send_to_server(format!("ping 12345"));

                    },
                    [Token::String(r)] => {
                        if r == "NOK" {
                            log_warning!("We've been timedout");
                            break;
                        }
                    },
                    _ => ()
                }

                drop(fishes_mutex);
            }
        }
    }
}