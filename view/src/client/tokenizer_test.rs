#[cfg(test)]
mod tests {
    use crate::client::tokenizer::Tokenizer;
    use crate::client::tokenizer::Token;


    #[test]
    fn test_string_with_integer() {
        let mut parser = Tokenizer::new();
        parser.parse(&mut "Lorem Ipsum       Dolor  4 ");
        assert_eq!(parser.tokens.len(), 4);

        match &parser.tokens[0] {
            Token::String(s) => assert_eq!(s, "Lorem"),
            _ => assert!(false),
        }
        match &parser.tokens[1] {
            Token::String(s) => assert_eq!(s, "Ipsum"),
            _ => assert!(false),
        }
        match &parser.tokens[2] {
            Token::String(s) => assert_eq!(s, "Dolor"),
            _ => assert!(false),
        }
        match &parser.tokens[3] {
            Token::Number(n) => assert_eq!(*n, 4),
            _ => assert!(false),
        }
    }

    #[test]
    fn test_complex_integers() {
        let mut parser = Tokenizer::new();
        parser.parse(&mut "100x100");
        assert_eq!(parser.tokens.len(), 3);

        match &parser.tokens[0] {
            Token::Number(n) => assert_eq!(*n, 100),
            _ => assert!(false),
        }
        match &parser.tokens[1] {
            Token::Cross => assert!(true),
            _ => assert!(false),
        }
        match &parser.tokens[2] {
            Token::Number(n) => assert_eq!(*n, 100),
            _ => assert!(false),
        }
    }

    #[test]
    fn test_cross_parsing() {
        let mut parser = Tokenizer::new();
        parser.parse(&mut "TestxxT 400x100 20xT Tx30");
        assert_eq!(parser.tokens.len(), 6);
        match &parser.tokens[0] {
            Token::String(s) => assert_eq!(s, "TestxxT"),
            _ => assert!(false),
        }
        match &parser.tokens[1] {
            Token::Number(n) => assert_eq!(*n, 400),
            _ => assert!(false),
        }
        match &parser.tokens[2] {
            Token::Cross => assert!(true),
            _ => assert!(false),
        }
        match &parser.tokens[3] {
            Token::Number(n) => assert_eq!(*n, 100),
            _ => assert!(false),
        }
        match &parser.tokens[4] {
            Token::String(s) => assert_eq!(s, "20xT"),
            _ => assert!(false),
        }
        match &parser.tokens[5] {
            Token::String(s) => assert_eq!(s, "Tx30"),
            _ => assert!(false),
        }
    }

    #[test]
    fn test_list_parsing1() {
        let mut parser = Tokenizer::new();
        parser.parse(&mut "list [PoissonxXxRouge at -90x-4,10x4,-5] [ PoissonClown at 20x-80, 12x6, 109000 ]");
        assert_eq!(parser.tokens.len(), 3);
        match &parser.tokens[0] {
            Token::ListKeyWord => assert!(true),
            _ => assert!(false),
        }
        match &parser.tokens[1] {
            Token::ListItem(li) => {
                assert_eq!(li.len(), 9);
                match &li[0] {
                    Token::String(s) => assert_eq!(s, "PoissonxXxRouge"),
                    _ => assert!(false),
                }
                match &li[1] {
                    Token::String(s) => assert_eq!(s, "at"),
                    _ => assert!(false),
                }
                match &li[2] {
                    Token::Number(n) => assert_eq!(*n, -90),
                    _ => assert!(false),
                }
                match &li[3] {
                    Token::Cross => assert!(true),
                    _ => assert!(false),
                }
                match &li[4] {
                    Token::Number(n) => assert_eq!(*n, -4),
                    _ => assert!(false),
                }
                match &li[5] {
                    Token::Number(n) => assert_eq!(*n, 10),
                    _ => assert!(false),
                }
                match &li[6] {
                    Token::Cross => assert!(true),
                    _ => assert!(false),
                }
                match &li[7] {
                    Token::Number(n) => assert_eq!(*n, 4),
                    _ => assert!(false),
                }
                match &li[8] {
                    Token::Number(n) => assert_eq!(*n, -5),
                    _ => assert!(false),
                }
            }
            _ => assert!(false),
        }
        match &parser.tokens[2] {
            Token::ListItem(li) => {
                assert_eq!(li.len(), 9);
                match &li[0] {
                    Token::String(s) => assert_eq!(s, "PoissonClown"),
                    _ => assert!(false),
                }
                match &li[1] {
                    Token::String(s) => assert_eq!(s, "at"),
                    _ => assert!(false),
                }
                match &li[2] {
                    Token::Number(n) => assert_eq!(*n, 20),
                    _ => assert!(false),
                }
                match &li[3] {
                    Token::Cross => assert!(true),
                    _ => assert!(false),
                }
                match &li[4] {
                    Token::Number(n) => assert_eq!(*n, -80),
                    _ => assert!(false),
                }
                match &li[5] {
                    Token::Number(n) => assert_eq!(*n, 12),
                    _ => assert!(false),
                }
                match &li[6] {
                    Token::Cross => assert!(true),
                    _ => assert!(false),
                }
                match &li[7] {
                    Token::Number(n) => assert_eq!(*n, 6),
                    _ => assert!(false),
                }
                match &li[8] {
                    Token::Number(n) => assert_eq!(*n, 109000),
                    _ => assert!(false),
                }
            }
            _ => assert!(false),
        }
    }

    #[test]
    fn test_list_parsing2() {
        let mut parser = Tokenizer::new();
        parser.parse(&mut "list [Arn at 28x166,1x1,0] [Arn at 82x55,1x1,5]");
        assert_eq!(parser.tokens.len(), 3);
        match &parser.tokens[0] {
            Token::ListKeyWord => assert!(true),
            _ => assert!(false),
        }
        match &parser.tokens[1] {
            Token::ListItem(li) => {
                assert_eq!(li.len(), 9);
                match &li[0] {
                    Token::String(s) => assert_eq!(s, "Arn"),
                    _ => assert!(false),
                }
                match &li[1] {
                    Token::String(s) => assert_eq!(s, "at"),
                    _ => assert!(false),
                }
                match &li[2] {
                    Token::Number(n) => assert_eq!(*n, 28),
                    _ => assert!(false),
                }
                match &li[3] {
                    Token::Cross => assert!(true),
                    _ => assert!(false),
                }
                match &li[4] {
                    Token::Number(n) => assert_eq!(*n, 166),
                    _ => assert!(false),
                }
                match &li[5] {
                    Token::Number(n) => assert_eq!(*n, 1),
                    _ => assert!(false),
                }
                match &li[6] {
                    Token::Cross => assert!(true),
                    _ => assert!(false),
                }
                match &li[7] {
                    Token::Number(n) => assert_eq!(*n, 1),
                    _ => assert!(false),
                }
                match &li[8] {
                    Token::Number(n) => assert_eq!(*n, 0),
                    _ => assert!(false),
                }
            }
            _ => assert!(false),
        }
        match &parser.tokens[2] {
            Token::ListItem(li) => {
                assert_eq!(li.len(), 9);
                match &li[0] {
                    Token::String(s) => assert_eq!(s, "Arn"),
                    _ => assert!(false),
                }
                match &li[1] {
                    Token::String(s) => assert_eq!(s, "at"),
                    _ => assert!(false),
                }
                match &li[2] {
                    Token::Number(n) => assert_eq!(*n, 82),
                    _ => assert!(false),
                }
                match &li[3] {
                    Token::Cross => assert!(true),
                    _ => assert!(false),
                }
                match &li[4] {
                    Token::Number(n) => assert_eq!(*n, 55),
                    _ => assert!(false),
                }
                match &li[5] {
                    Token::Number(n) => assert_eq!(*n, 1),
                    _ => assert!(false),
                }
                match &li[6] {
                    Token::Cross => assert!(true),
                    _ => assert!(false),
                }
                match &li[7] {
                    Token::Number(n) => assert_eq!(*n, 1),
                    _ => assert!(false),
                }
                match &li[8] {
                    Token::Number(n) => assert_eq!(*n, 5),
                    _ => assert!(false),
                }
            }
            _ => assert!(false),
        }
    }
}