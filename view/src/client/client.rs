use std::error;
use std::fmt;
use std::io::BufRead;
use std::io::BufReader;
use std::sync::{ Arc, Mutex };
use std::thread::scope;
use std::collections::HashMap;
use std::{ io::Write, net::TcpStream };

use crate::client::view::View;
use crate::prelude::*;
use crate::utils::get_config;
use crate::utils::ConfigParser;

use super::tokenizer::Token;
use super::tokenizer::Tokenizer;
use super::PromptHandler;
pub struct Instruction {
    pub position_end: (f64, f64),
    pub position_start: (f64, f64),
    pub size_end: (i32, i32),
    pub duration: i32,
    pub start_date: Option<std::time::Instant>,
}

pub struct Fish {
    pub name: String,
    pub position_current: (f64, f64),
    pub instructions: Vec<Instruction>,
}

pub struct Client {
    pub stream: Arc<Mutex<Option<TcpStream>>>,
    pub prompter: Arc<Mutex<Option<PromptHandler>>>,
    pub view_id: Arc<Mutex<Option<String>>>,
    pub config: ConfigParser,
    pub fishes: Arc<Mutex<HashMap<String, Fish>>>,

}

#[derive(Debug, Clone)]
struct MissingTcpStream;

impl fmt::Display for MissingTcpStream {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "Client is missing a TcpStream to server")
    }
}

impl error::Error for MissingTcpStream {}

impl Client {
    pub fn new() -> Client {
        return Client {
            stream: Arc::new(Mutex::new(None)),
            prompter: Arc::new(Mutex::new(None)),
            config: get_config(),
            view_id: Arc::new(Mutex::new(Option::None)),
            fishes: Arc::new(Mutex::new(HashMap::new())),
        };
    }

    pub fn run(&mut self) {
        log_debug!("Debug mode activated.");
        log_info!("Starting client..");

        self.connect();

        scope(|s| {

            s.spawn(|| {
                self.start_prompter();

                let stream = Arc::clone(&self.stream);
                let mut stream_mutex = stream.lock().unwrap();
                // Closing procedure (ie: shutdown TcpStream)
                match &mut *stream_mutex {
                    None => log_warning!("TcpStream not found while closing client"),
                    Some(stream) =>
                        match stream.shutdown(std::net::Shutdown::Read) {
                            Err(e) => log_warning!("Failed to shutdown TcpStream: {e}"),
                            Ok(_) => log_success!("Client successfully terminated."),
                        }
                }
            });

            View::new(self.fishes.clone()).start();
        });
    }


    pub fn wait_one_server_answer(&self) -> Result<String, Box<dyn error::Error>> {
        let mut buf = String::new();

        let stream = Arc::clone(&self.stream);
        let mut stream_mutex = stream.lock().unwrap();

        return match &mut *stream_mutex {
            None => { Err(Box::new(MissingTcpStream)) }
            Some(stream) =>
                match BufReader::new(stream).read_line(&mut buf) {
                    Err(e) => {
                        log_error!("Can't peek on server tcp stream: {e}");
                        Err(Box::new(e))
                    }
                    Ok(_len) => Ok(buf),
                }
        };
    }

    pub fn connect(&self) {
        let add = get_config().address + ":" + &get_config().port.to_string();
        log_debug!("{add}");
        match TcpStream::connect(get_config().address + ":" + &get_config().port.to_string()) {
            Ok(r) => {
                let stream = Arc::clone(&self.stream);
                let mut stream_mutex = stream.lock().unwrap();

                *stream_mutex = Some(r);
                log_success!("Client is now connected to server");
            }
            Err(r) => log_warning!("Can't connect to server, please retry later : {r}"),
        }
    }

    fn start_prompter(&self) {
        Self::welcome_message(); // Display to the user help messages
        let prompter = Arc::clone(&self.prompter);
        let mut prompter_mutex = prompter.lock().unwrap();
        *prompter_mutex = Some(PromptHandler::new());

        prompter_mutex
            .as_mut()
            .unwrap()
            .handle_input(|prompter, elements| self.cmd_rooter(elements, prompter));
    }

    pub fn send_to_server(&self, request: String) {
        let stream = Arc::clone(&self.stream);
        let mut stream_mutex = stream.lock().unwrap();

        match &mut *stream_mutex {
            None => log_error!("Can't find server tcp stream"),
            Some(stream) =>
                match stream.write_all((request + "\n").as_bytes()) {
                    Err(e) => log_error!("While writing to server tcp stream: {e}"),
                    Ok(_) => log_debug!("Message sent to server!"),
                }
        }
    }

    pub fn consume_answer(&self) {
        match self.wait_one_server_answer() {
            Ok(s) => log_info!("Server -> {s}"),
            Err(r) => log_error!("Error while consuming a server answer : {r}"),
        }
    }

    /**
     * @brief Depending on the cmd name, the function will root it to a specific cmd handler.
     */
    fn cmd_rooter(&self, tokenizer: Tokenizer, prompter: &mut PromptHandler) {
        if tokenizer.tokens.len() == 0 {
            return log_warning!("Please do provide a non-empty command.");
        }
        match &tokenizer.tokens[..] {
            [Token::String(cmd), ..] =>
                match (cmd.as_str(), &tokenizer.tokens[1..]) {
                    ("status", []) => self.handle_cmd_status(),
                    ("connect", []) => self.handle_cmd_connect(),
                    ("autosync", []) => self.handle_cmd_autosync(),
                    ("login", []) => self.handle_cmd_login(),
                    ("quit", []) => self.handle_cmd_quit(prompter),
                    (
                        "addFish",
                        [
                            Token::String(fish_name),
                            Token::String(_),
                            Token::Number(x),
                            Token::Cross,
                            Token::Number(y),
                            Token::Number(w),
                            Token::Cross,
                            Token::Number(h),
                            Token::String(method),
                        ],
                    ) => self.handle_cmd_add_fish(fish_name, method, x, y, w, h),
                    ("delFish", [Token::String(fish_name)]) => self.handle_cmd_del_fish(fish_name),
                    ("startFish", [Token::String(fish_name)]) =>
                        self.handle_cmd_start_fish(fish_name),
                    _ =>
                        log_error!(
                            "This command is not recognized, please type help to find every available command."
                        ),
                }
            _ =>
                log_error!(
                    "This command is not recognized, please type help to find every available command."
                ),
        }
    }
}