extern crate piston;
use std::{
    collections::HashMap,
    sync::{Arc, Mutex},
    time::Instant, fs,
};

use piston::Window;
use piston_window::{types::Color, *};


use crate::{log_debug, log_warning};

use super::client::Fish;

pub struct View {
    p_view: PistonWindow,
    fishes: Arc<Mutex<HashMap<String, Fish>>>,
    textures: HashMap<String, G2dTexture>,
}


impl View {
    pub fn new(fishes: Arc<Mutex<HashMap<String, Fish>>>) -> View {
        return View {
            p_view: WindowSettings::new("Aquarium", [1000, 1000])
                .exit_on_esc(true)
                .graphics_api(OpenGL::V4_5)
                .build()
                .unwrap(),
            fishes: fishes,
            textures: HashMap::new(),
        };
    }

    pub fn draw_text(
        ctx: &Context,
        graphics: &mut G2d,
        glyphs: &mut Glyphs,
        color: Color,
        x: f64,
        y: f64,
        text: &str,
    ) {
        text::Text::new_color(color, 30)
            .draw(
                text,
                glyphs,
                &ctx.draw_state,
                ctx.transform.trans(x, y),
                graphics,
            )
            .unwrap();
    }

    fn load_textures(&mut self) {
        let assets = find_folder::Search::ParentsThenKids(3, 3)
            .for_folder("assets")
            .unwrap();

        let background_texture: G2dTexture = Texture::from_path(
            &mut self.p_view.create_texture_context(),
            &assets.join("background.jpg"),
            Flip::None,
            &TextureSettings::new(),
        )
        .unwrap();


        self.textures
            .insert(String::from("background"), background_texture);



        let fish_textures = fs::read_dir(&assets.join("fishes")).unwrap();

        for fish_texture_path in fish_textures {
            let fish_texture_dir_entry = fish_texture_path.unwrap();

            let fish_texture: G2dTexture = Texture::from_path(
                &mut self.p_view.create_texture_context(),
                &fish_texture_dir_entry.path(),
                Flip::None,
                &TextureSettings::new(),
            )
            .unwrap();
            let os_name = fish_texture_dir_entry.file_name();
            let name_with_extension = os_name.to_str().unwrap();
            let splitted_name = name_with_extension.split('.').collect::<Vec<&str>>();

            match splitted_name.len() {
                2 => {
                    self.textures.insert(String::from(splitted_name[0]), fish_texture);
                },
                _ => log_warning!("An invalid texture is present in fish texture folder.")
            }

        }

    }

    fn setup(&mut self) {
        self.p_view.set_lazy(false);

        self.load_textures();
    }

    fn draw_background(&mut self, e: &Event) {
        let p_size = self.p_view.size();
        self.p_view.draw_2d(e, |c, g, _| {
            let background_texture = self.textures.get("background").unwrap();
            clear([1.0; 4], g);
            image(
                background_texture,
                c.transform.scale(
                    p_size.width / f64::from(background_texture.get_size().0),
                    1.0,
                ),
                g,
            );
        });
    }

    fn draw_fishes(&mut self, e: &Event) {
        let mut fishes_mutex = self.fishes.lock().unwrap();
        let p_size = self.p_view.size();

        self.p_view.draw_2d(e, |c, g, _| {
            let mut i = 0;
            fishes_mutex.iter_mut().for_each(|f| {
                i += 1;

                if f.1.instructions.len() > 0 {
                    let mut current_instruction = &mut f.1.instructions[0];
                    let elapsed = match current_instruction.start_date {
                        Some(r) => r.elapsed().as_secs_f64() as f64,
                        None => 0.0,
                    };
                    let duration = f64::from(current_instruction.duration);
                    match current_instruction.start_date {
                        Some(_) => (),
                        None => current_instruction.start_date = Some(Instant::now()),
                    }

                    if current_instruction.position_start == (-1.0, -1.0) {
                        current_instruction.position_start = f.1.position_current;
                    }

                    if duration > 0.0 {
                        let delta_d_x = (current_instruction.position_end.0
                            - current_instruction.position_start.0);
                        let delta_d_y = (current_instruction.position_end.1
                            - current_instruction.position_start.1);
                        f.1.position_current.0 =
                            elapsed / duration * delta_d_x + current_instruction.position_start.0;
                        f.1.position_current.1 =
                            elapsed / duration * delta_d_y + current_instruction.position_start.1;
                    } else {
                        f.1.position_current = current_instruction.position_end;
                    }

                    if f.1.position_current.0 > 0.0
                        && f.1.position_current.1 > 0.0
                        && f.1.position_current.0 <= 100.0
                        && f.1.position_current.1 <= 100.0
                    {

                        
                        let fish_texture = self.textures.get(f.0).unwrap_or(self.textures.get("magicarpe").unwrap());
                        let sx = (f64::from(current_instruction.size_end.0) * p_size.width / 100.0)
                            / f64::from(fish_texture.get_size().0);
                        let sy = (f64::from(current_instruction.size_end.1) * p_size.height
                            / 100.0)
                            / f64::from(fish_texture.get_size().1);

                        let trans_x = (f.1.position_current.0 * p_size.width) / 100.0;
                        let trans_y = (f.1.position_current.1 * p_size.height) / 100.0;
                        image(
                            fish_texture,
                            c.transform.trans(trans_x, trans_y).scale(sx, sy),
                            g,
                        );
                    }

                    if elapsed >= duration {
                        f.1.instructions.remove(0);
                    }
                }
            });
            drop(fishes_mutex);

        });

    }

    pub fn start(&mut self) {
        self.setup();
        while let Some(e) = self.p_view.next() {
            self.draw_background(&e);


            self.draw_fishes(&e);
        }
    }
}
