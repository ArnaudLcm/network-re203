extern crate chrono;

use color_print::cprintln;
use chrono::Local;

static CURRENT_LVL: u32 = 0;

pub fn _log(input: String, color: &str, lvl: u32, head: &str) {
    if CURRENT_LVL > lvl {
        return;
    } else {
        let date = Local::now();
        cprintln!(
            "{0}{1} [{2}] {3}\u{1b}[39m\n",
            color,
            date.format("[%Y-%m-%d %H:%M:%S]"),
            head,
            input
        );
    }
}

#[macro_export]
macro_rules! log_success {
    ($input:expr) => {
        {
            $crate::utils::logger::_log(format!($input), "\u{1b}[32m", 0, "SUCCESS");
        }
    };
}
pub(crate) use log_success;

#[macro_export]
macro_rules! log_info {
    ($input:expr) => {
        {
            $crate::utils::logger::_log(format!($input), "\u{1b}[34m", 1, "INFO");
        }
    };
}
pub(crate) use log_info;

#[macro_export]
macro_rules! log_warning {
    ($input:expr) => {
        {
            $crate::utils::logger::_log(format!($input), "\u{1b}[33m", 2, "WARNING");
        }
    };
}
pub(crate) use log_warning;

#[macro_export]
macro_rules! log_error {
    ($input:expr) => {
        {
            $crate::utils::logger::_log(format!($input), "\u{1b}[31m", 3, "ERROR");
        }
    };
}
pub(crate) use log_error;

#[macro_export]
macro_rules! log_debug {
    ($input:expr) => {
        {
            $crate::utils::logger::_log(format!($input), "\u{1b}[35m", 4, "DEBUG");
        }
    };
}
pub(crate) use log_debug;