use serde::{Serialize, Deserialize};
use config_file::FromConfigFile;

#[derive(Debug, Serialize, Deserialize, Default)]
pub struct ConfigParser {
    pub address: String,
    pub port: usize,
    pub id: String,
    pub timeout: usize,
    pub resources: String
}

pub fn get_config() -> ConfigParser {
    let config = ConfigParser::from_config_file("display.toml").unwrap();
    return config;
}