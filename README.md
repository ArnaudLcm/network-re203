# network-re203

## Requirements

Please check you fill every requirements :

- Install [rust](https://doc.rust-lang.org/cargo/getting-started/installation.html)
- Install [make](https://www.gnu.org/software/make/manual/make.html)
- Install [gcc](https://gcc.gnu.org/install/)


## Install libs
This project relies on a [socket server](https://gitlab.com/ArnaudLcm/c-eventpoll-webserver) we built few times ago.
In order to make the controller part work, you have to install this library.
Thus, you have to clone the repository with the follow option :
```bash
git clone --recurse-submodules https://gitlab.com/ArnaudLcm/network-re203.git
```

## Compile the project

In order to compile the project, you have to do it for both controller and view.
i
### Compile the controller

At the root of the project, please follow those instructions :

```bash
cd controller
make build
```

### Compile the view

At the root of the project, please follow those instructions :

```bash
cd view
cargo build
```
